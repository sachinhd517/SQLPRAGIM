--SELF JOIN IN SQL SERVER - PART 14

select * from tbl_EmployeePart14

CREATE TABLE tbl_EmployeePart14
(
	[EmployeeID] int IDENTITY NOT NULL,
	[Name] nvarchar(50) NOT NULL,
	ManagerID nvarchar(50)
)

insert into tbl_EmployeePart14 values('Mike',3)
insert into tbl_EmployeePart14 values('ROb',1)
insert into tbl_EmployeePart14 values('Todd',NULL)
insert into tbl_EmployeePart14 values('Ben',1)
insert into tbl_EmployeePart14 values('Sam',1)

select ISNULL(NULL,'No Manager') as Manager

select COALESCE(NULL,'No Manager') as Manager

CASE WHEN Expression THEN '' ELSE '' END

select		E.Name as Employee, M.Name as Manager
from		tbl_EmployeePart14 E
LEFT JOIN	tbl_EmployeePart14 M
ON			E.EmployeeID = M.ManagerID



select		E.Name as Employee, M.Name as Manager
from		tbl_EmployeePart14 E
CROSS JOIN	tbl_EmployeePart14 M

select		E.Name as Employee, M.Name as Manager
from		tbl_EmployeePart14 E
RIGHT JOIN	tbl_EmployeePart14 M
ON			E.EmployeeID = M.ManagerID

select				E.Name as Employee, M.Name as Manager
from				tbl_EmployeePart14 E
RIGHT OUTER JOIN	tbl_EmployeePart14 M
ON					E.EmployeeID = M.ManagerID


select		E.Name as Employee, ISNULL(M.Name,'No Manager') as Manager
FROM		tbl_EmployeePart14 E
LEFT JOIN	tbl_EmployeePart14 M
ON			E.ManagerID = M.EmployeeID

select		E.Name as Employee, COALESCE(M.Name,'No Manager') as Manager
FROM		tbl_EmployeePart14 E
LEFT JOIN	tbl_EmployeePart14 M
ON			E.ManagerID = M.EmployeeID


select		E.Name as Employee, CASE WHEN M.Name IS NULL THEN 'No Manager' ELSE M.Name END as Manager
FROM		tbl_EmployeePart14 E
LEFT JOIN	tbl_EmployeePart14 M
ON			E.ManagerID = M.EmployeeID

select		E.Name as Employee, M.Name as Manager
FROM		tbl_EmployeePart14 E
CROSS JOIN  tbl_EmployeePart14 M

select		E.Name as Employee, M.Name as Manager
FROM		tbl_EmployeePart14 E
LEFT JOIN	tbl_EmployeePart14 M
ON			E.ManagerID = M.EmployeeID

SELECT		E.Name as Employee, M.Name as Manager
FROM		tbl_EmployeePart14 E
LEFT JOIN	tbl_EmployeePart14 M
ON			E.EmployeeID = M.ManagerID

select * from tbl_EmployeePart14

