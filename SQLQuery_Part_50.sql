----Is it possible to UPDATE a CTE - Yes & No

--So, if a CTE is created on one base table, then it is possibnle to UPDATE the CTE, which in
--turn will update the underlying base table.

--In this case, UPDATING Employees_Name_Gender CTE, Update tblEmployee table.

--Updatable CTE

--1.	If a CTE is based on a single base table, the  the UPDATE succeeds and works as expected.

--2.	If a CTE is based on more than on base table, and if the UPDATE affects multiple base tables, 
--		the updage to snot allowed and the statment terminates
--4.	If a CTE is based on more than one base table, and if the UPDATE affected only one base table,
--		the UPDATE succeeds(but not as expected always) 


CREATE TABLE tblEmployee50
(
  Id int Primary Key,
  Name nvarchar(30),
  Salary int,
  Gender nvarchar(10),
  DepartmentId int
)


Insert into tblEmployee50 values (1,'John', 5000, 'Male', 3)
Insert into tblEmployee50 values (2,'Mike', 3400, 'Male', 2)
Insert into tblEmployee50 values (3,'Pam', 6000, 'Female', 1)
Insert into tblEmployee50 values (4,'Todd', 5000, 'Male', 4)  
Insert into tblEmployee50 values (5,'Sara', 3200, 'Female', 1) 
Insert into tblEmployee50 values (6,'Ben', 5000, 'Female', 3) 

CREATE TABLE tblDepartment50
(
  DeptId int identity(1,1) primary key,
  DepartmentName nvarchar(1000)
)

Insert into tblDepartment50 values ('IT')
Insert into tblDepartment50 values ('Payroll')
Insert into tblDepartment50 values ('HR')
Insert into tblDepartment50 values ('Admin')

select * from tblEmployee50 
select * from tblDepartment50

ALTER TABLE tblEmployee50 add constraint FK_tblEmployee50_DepartmentId
FOREIGN KEY (DepartmentId)  references tblDepartment50(DeptId)

--CTE WITH one base table

with tblEmployee50_Name_Gender
as
(
	select Id, Name, Gender from tblEmployee50
)
select * from tblEmployee50_Name_Gender


WITH tblEmployee50_Name_Gender50
as
(
	select Id, Name, Gender from tblEmployee50
)
Update tblEmployee50_Name_Gender50 set Gender = 'Female' where Id = 1

select * from tblEmployee50

--CTE on 2 base tables
---------------------------------------------------------------------------

WITH EmployeesByDepartment
as
(
		SELECT		Id, Name, Gender, DepartmentName
		FROM		tblEmployee50
		JOIN		tblDepartment50
		ON			tblDepartment50.DeptId	= tblEmployee50.DepartmentId 	
)
select * from EmployeesByDepartment
--===========================================================================

WITH EmployeesByDepartment
as
(
		SELECT		Id, Name, Gender, DepartmentName
		FROM		tblEmployee50
		JOIN		tblDepartment50
		ON			tblDepartment50.DeptId = tblEmployee50.DepartmentId
)
Update EmployeesByDepartment set Gender = 'Male' where Id = 1

--===========================================================================

--CTE on 2 base tables, update affecting more then one base table

WITH EmployeesByDepartment
as
(
		SELECT		Id, Name, Gender, DepartmentName
		FROM		tblEmployee50
		JOIN		tblDepartment50
		ON			tblDepartment50.DeptId = tblEmployee50.DepartmentId
)
Update EmployeesByDepartment set Gender = 'Female', DepartmentName = 'IT' where Id = 1

--Error
--Msg 4405, Level 16, State 1, Line 89
--View or function 'EmployeesByDepartment' is not updatable because the modification affects multiple base tables.

--=================================================================================

select @@CONNECTIONS
select @@IDENTITY
select @@SERVERNAME
select @@LANGID
select @@SERVICENAME
select @@VERSION