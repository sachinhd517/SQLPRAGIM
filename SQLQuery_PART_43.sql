--DML Trigger in sql server 

--Triggers

--In SQL sever there are 3 types of triggers

--1.	DML triggers
--2.	DDL triggers
--3.	Logon trigger

--DML triggers are fired automatically in response to DML events(INSERT,UPDATE & DELETE)

--DML triggers can be again classified into 2 types

--1.	After triggers (Sometimes called as FORtriggers)
--2.	Instead of triggers

--After triggers, fries after the triggering action. The INSERT,UPDATE, and DELETE
--statment, causes an after trigger to fire after respective statments complete
--execution.

--INSTEAD of trigger, fires instead of the triggering action. The INSERT, UPDATE, and 
--DELETE statments, causes an INSTEAD OF trigger to fire INSTEAD of the respective
--statments execution.

CREATE TABLE tblEmployee43
(
  Id int Primary Key,
  Name nvarchar(30),
  Salary int,
  Gender nvarchar(10),
  DepartmentId int
)


Insert into tblEmployee43 values (1,'John', 5000, 'Male', 3)
Insert into tblEmployee43 values (2,'Mike', 3400, 'Male', 2)
Insert into tblEmployee43 values (3,'Pam', 6000, 'Female', 1)
Insert into tblEmployee43 values (4,'Todd', 4800, 'Male', 4)  
Insert into tblEmployee43 values (5,'Sara', 3200, 'Female', 1) 
Insert into tblEmployee43 values (6,'Ben', 4800, 'Female', 3) 

CREATE TABLE tblEmployeeAudit43
(
  Id int identity(1,1) primary key,
  AuditData nvarchar(1000)
)

Insert into tblEmployeeAudit43 values ('IT')
Insert into tblEmployeeAudit43 values ('Payroll')
Insert into tblEmployeeAudit43 values ('HR')
Insert into tblEmployeeAudit43 values ('Admin')

select * from tblEmployee43 
select * from tblEmployeeAudit43

ALTER TABLE tblEmployee43 add constraint FK_tblEmployee43_DepartmentId
FOREIGN KEY (DepartmentId) references tblEmployeeAudit43(Id)

CREATE TRIGGER Tr_tblEmployee_ForInsert
ON tblEmployee43
FOR INSERT
AS
BEGIN
	select * from inserted
END

insert into tblEmployee43 values(8,'Jane',1800,'Female',3)
insert into tblEmployee43 values(9,'Jimy',1800,'Female',3)
select * from inserted

--AFTER INSERT TRIGGERS

CREATE TRIGGER Tr_tblEmployees_ForInsert
ON tblEmployee43
FOR INSERT
AS
BEGIN

	Declare @Id int
	select @Id = Id from inserted

	insert into tblEmployeeAudit43
	values('New Employee with ID = ' + Cast(@ID as nvarchar(50)) + ' is added at ' + Cast(GetDate() as nvarchar(20) ) )
END

--AFTER DELETE TRIGGERS

CREATE TRIGGER Tr_tblEmployee_ForDeleted
ON tblEmployee43
FOR DELETE
AS
BEGIN

	Declare @Id int
	select @Id = Id from deleted 

	insert into tblEmployeeAudit43
	values('New Employee with ID = ' + Cast(@Id as nvarchar(50)) + ' is deleted at ' + Cast(Getdate() as nvarchar(20) ) )
END


Create Trigger tr_tblEmployee43_forUpdate
on tblEmployee43
for Update
as
Begin
	select * from deleted
	select * from inserted
End


update tblEmployee43 set Name = 'James',Salary = 2000, Gender = 'Male' where Id = 8

delete from tblEmployee43 where Id = 2

CREATE TRIGGER Trg_tblEmployee43_ForUpdate
ON tblEmployee43
FOR Update
AS
BEGIN
		Declare @Id int
		Declare @OldName nvarchar(50), @NewName nvarchar(50)
		Declare @OldSalary int, @NewSalary int
		Declare @OldGender nvarchar(50), @NewGender nvarchar(50)
		Declare @OldDepartmentId int, @NewDepartmentId int

		Declare @AuditString nvarchar(2000)

		select * into #TempTable43
		from inserted

		while(Exists(select Id from #TempTable43))
		Begin
			
			set @AuditString = ''

			select TOP 1 @Id = Id, @NewName = Name,
			@NewSalary = Salary, @NewGender = Gender,
			@NewDepartmentId = DepartmentId from #TempTable43

			select @OldName = Name, @OldGender = Gender,
			@OldSalary = Salary, @OldDepartmentId = DepartmentID 
			from deleted where Id = @Id

			set @AuditString = 'Employee With Id' + CAST(@Id as nvarchar(50)) + ' Changed '
			if(@OldName <> @NewName)
				set @AuditString = @AuditString + ' Name from ' + @OldName + ' to ' + @NewName
			if(@OldSalary <> @NewSalary)
				set @AuditString = @AuditString + ' Gender from ' + @OldGEnder + ' to ' + @NewGender
			if(@OldSalary <> @NewSalary)
				set @AuditString = @AuditString + ' Salary from ' + cast( @OldSalary as nvarchar(100)) + ' to ' + Cast(@NewSalary as nvarchar(400))
			if(@OldDepartmentId <> @NewDepartmentId)
				set @AuditString = @AuditString + ' Department Id from '+ CAST(@OldDepartmentId as nvarchar(100)) + ' to ' + CAST(@NewDepartmentId as nvarchar(500))
			insert into tblEmployee43 values (@AuditString)

			Delete from #TempTable43 where Id = @Id
		END
END