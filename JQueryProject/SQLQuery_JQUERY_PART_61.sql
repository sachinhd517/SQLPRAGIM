Create table tblEmployee
(
	[Id] int primary key Identity,
	[Name] nvarchar(50),
	[Gender] nvarchar(10),
	[Salary] int
)

insert into tblEmployee values('Mark','Male',50000)
insert into tblEmployee values('Sara','Female',60000)
insert into tblEmployee values('Jhon','Male',45000)
insert into tblEmployee values('Pam','Female',50000)

select * from tblEmployee

Create Procedure spGetEmployeeById 
@Id int
as
Begin
	select ID,Name,Gender,Salary from tblEmployee where ID = @Id
END