Create table tblHelpText
(
	HelpTextKey nvarchar(50) primary key,
	HelpText nvarchar(250)
)

Insert into tblHelpText values
('firstName','Your fisrt name as it appears in passport')
Insert into tblHelpText values
('lastName','Your last name as it appears in passport')
Insert into tblHelpText values
('email','Your email address for communication')
Insert into tblHelpText values
('income','Your annual income')


Create procedure spGetHelpTextByKey 
@HelpTextKey nvarchar(50)
as
Begin
	Select HelpText from tblHelpText where  HelpTextKey=@HelpTextKey
END