--Subqueries in sql server

--Example 1:
--Write query to retrieve products that are not at all sold?

--Example 2:
--Write query to retrieve the Name and TOTALQUANTITY sold?

--From these example, it should be very clear that. a subquery is imply a select
--statment, that returns a single value and can be nested inside a SELECT, UPDATE,
--INSERT, or DELETE statment. It is also possible to nest a subquery inside another
--subquery. According to MSDN, subqueries can be nested upto 32 levels.

--Subsquery are always enclosed in paranthesis and are also caleed as inner quenes,
--and the query containing the subquery is called as outer query. The columns from a
--table that ispresent only inside a subquery, cannot be used in the SELECT list of the
--outer query.


Create table tblProducts59
(
	[Id] int primary key identity,
	[Name] nvarchar(50),
	[Description] nvarchar(250)
)

Create Table tblProductSales59
(
	[Id] int primary key identity,
	[ProductsId] int foreign key references tblProducts59(Id),
	[unitPrice] int,
	[QuantitySold] int
)

insert into tblProducts59 values('TV','52 inch black color LCD TV')
insert into tblProducts59 values('Laptop','Very thin blackcolor acer laptop')
insert into tblProducts59 values('Desktop','HP high performance desktop')

insert into tblProductSales59 values(3,450,5)
insert into tblProductSales59 values(2,250,7)
insert into tblProductSales59 values(3,450,4)
insert into tblProductSales59 values(3,450,9)

select * from tblProducts59
select * from tblProductSales59

select		Id, Name,[Description]
from		tblProducts59
where		Id NOT IN(select Distinct ProductsId from tblProductSales59)

select		tblProducts59.Id,Name,[Description]
from		tblProducts59
LEFT JOIN	tblProductSales59
ON			tblProducts59.Id = tblProductSales59.ProductsId
where		tblProductSales59.ProductsId IS NULL


select		Name,
(select SUM(QuantitySold) from tblProductSales59 where ProductsId = tblProducts59.Id) as QtySold
from		tblProducts59
order By Name

select		Name, SUM(QuantitySold) as QtySold
from	    tblProducts59
left JOIN	tblProductSales59
ON			tblProducts59.Id = tblProductSales59.ProductsId
GROUP by Name