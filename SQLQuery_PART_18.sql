--Stored Procedures

--A stored procedure is group of Y-SQL(Transact SQL) statements. If you have a situation, where
--you write the same query over and over again, you can save that specific query as a stored procedure
--and call it just by it's name.

--1.	User CCREATE PROCEDURE or CREATE PROC statments to create SP

--Note : When naming user defined strored procedures, Microsoft recommends not to use sp_as a 
--prefix. ALL system stroed procedures, are prefix with sp_. This avoids any ambiguity between
--user defined and system stored procedure and any confilcts, with some future system procedure.

--To execute the stored procedure

--1.	spGetEmployees
--2.	EXEC spGetEmployees
--3.	Execute spGetEmployees

--Note : You can also right click on the procedure name, in object explorer in SQL Server
--Management Studio and select EXECUTE STORED PROCEDURE

--Stored Procedure with Parameters

--Parameters and variables have an @ prefix in their name.

--To Execute:

--EXECUTE spGetEmployee18ByGenderAndDepartment 'Male',1
--EXECUTE spGetEmployee18ByGenderAndDepartment @DepartmentId=1,@Gender='Male'

--To view the text, of the sotred procedure

--1.	Use system stored procedure sp_helptext 'spName'
--2.	Right Click the SP in Object explorer-> Script Procedure as-> New Query Editor
--		window.

--To change the store procedure, use ALTER PROCEDURE statement.

--To delete the SP, use DROP PROC 'SpName' or DROP PROCEDURE 'SPName'

--To encrypt the text of the SP, use WITH ENCRYPTION option. It is not possible to view the text of an encrypted SP.

--Next Session - Creating and Invoking a stored procedure with out put parameters

create table tblEmployee18
(
	[Id] int primary key NOT NULL,
	[Name] nvarchar(50) NOT NULL,
	[Gender] nvarchar(50) NOT NULL,
	[DepartmentId] int NOT NULL
)
Create table tblDepartment18
(
	[ID] int primary key,
	[Dept_Name] nvarchar(50)
)

insert into tblEmployee18 values(1,'sam','Male',1)
insert into tblEmployee18 values(2,'Ram','Male',1)
insert into tblEmployee18 values(3,'Sara','Female',3)
insert into tblEmployee18 values(4,'Todd','Male',2)
insert into tblEmployee18 values(5,'John','Male',3)
insert into tblEmployee18 values(6,'Sana','Female',2)
insert into tblEmployee18 values(7,'James','Male',1)
insert into tblEmployee18 values(8,'Rob','Male',2)
insert into tblEmployee18 values(9,'Steve','Male',1)
insert into tblEmployee18 values(10,'Pam','Female',2)


insert into tblDepartment18 values(1,'Devloper')
insert into tblDepartment18 values(2,'HR')
insert into tblDepartment18 values(3,'Account')


select * from tblEmployee18
select * from tblDepartment18

CREATE PROCEDURE spGetEmployees
as
BEGIN
	SELECT Name,Gender from tblEmployee18 
END

ALTER PROCEDURE spGetEmployees
as
BEGIN
	SELECT Name,Gender from tblEmployee18 order by Name 
END


DROP PROC spGetEmployees

sp_helptext spGetEmployees

exec spGetEmployees

execute spGetEmployees


create proc spGetEmployeeByGenderAndDepartment
@Gender nvarchar(50),
@DepartmentId int
as 
Begin
	select Name, Gender, DepartmentId from tblEmployee18 where Gender = @Gender and DepartmentId = @DepartmentId
END

spGetEmployeeByGenderAndDepartment 'Male',2

spGetEmployeeByGenderAndDepartment 2,'Male'

spGetEmployeeByGenderAndDepartment @DepartmentId = 2,@Gender ='Male'

sp_helptext spGetEmployeeByGenderAndDepartment

ALTER proc spGetEmployeeByGenderAndDepartment  
@Gender nvarchar(50),  
@DepartmentId int  
WITH Encryption
as   
Begin  
 select Name, Gender, DepartmentId from tblEmployee18 where Gender = @Gender and DepartmentId = @DepartmentId  
END

sp_helptext spGetEmployeeByGenderAndDepartment


spGetEmployeeByGenderAndDepartment 'Male',1

sp_helptext spGetEmployees



Alter table tblEmployee18 add constraint FK_tblEmployee18_GenderId
Foreign key (DepartmentId) references tblDepartment18(ID) 

CREATE PROCEDURE spGetEmployee18
as
Begin
	select * from tblEmployee18
END

exec spGetEmployee18

create proc spGetEmployee
as
Begin
	select Name,Gender from tblEmployee18
END

CREATE PROCEDURE spGetEmployee18ByGenderAndDepartment
@Gender nvarchar(50),
@DepartmentId int
as
Begin
	select Name, Gender, DepartmentId from tblEmployee18 where Gender = @Gender and DepartmentId = @DepartmentId
END
spGetEmployee18ByGenderAndDepartment 'Male',1

spGetEmployee18ByGenderAndDepartment @DepartmentId=1, @Gender='Male'

sp_helptext spGetEmployee18ByGenderAndDepartment


ALTER proc spGetEmployee
as
Begin
	select Name,Gender from tblEmployee18 order by Name
END

Drop procedure spGetEmployee18
Drop proc spGetEmployee18



ALTER PROCEDURE spGetEmployee18ByGenderAndDepartment
@Gender nvarchar(50),
@DepartmentId int
with ENCRYPTION
as
Begin
	select Name, Gender, DepartmentId from tblEmployee18 where Gender = @Gender and DepartmentId = @DepartmentId
END