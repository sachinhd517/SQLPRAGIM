-- DIFFERENT WAYS TO REPLACE NULL IN SQL SERVER PART 15


-- ISNULL EXAMPLE

select			E.Name as Employee, ISNULL(M.Name,'NO MANAGER') as Manager
FROM			tbl_Employee E
LEFT JOIN 		tbl_Employee M
ON				E.ManagerID = M.EmployeeID

SELECT ISNULL('SACHIN','NO MANAGER') AS Manager
---------------------------------------------------
-- COALESCE EXAMPLE

SELECT COALESCE('sachin','No Manager') as Manager

SELECT COALESCE(NULL,'NO MANAGER') as Manager

use [ASP_DATABASE]
GO

SELECT			E.Name as Employee, COALESCE(M.Name,'NO MANAGER') as Manager
FROM			tbl_Employee E
LEFT JOIN		tbl_Employee M
ON				E.ManagerID = M.EmployeeID

-----------------------------------------------------------------------------
-- CASE EXAMPLE

CASE WHEN EXPRESSION THEN '' ELSE '' END

SELECT			E.Name as Employee, CASE WHEN M.Name IS NULL THEN 'No Manager' ELSE M.Name END as Manager
FROM			tbl_Employee E
LEFT JOIN		tbl_Employee M
ON				E.ManagerID = M.EmployeeID

