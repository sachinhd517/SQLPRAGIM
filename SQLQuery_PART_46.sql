--Instead of update trigger in sql server Part - 46

--Triggers 

--In SQL server there are 3 types of triggers

--1.	DML triggers
--2.	DDL triggers
--3.	Logon triggers

--DML trigger are fired automatically in response to DML events(INSERT,UPDATE & DELETE)

--DML triggers can be again classified into 2 types

--1.	After triggers (Something called as FOR triggers)
--2.	Instead of triggers

--After triggers, fires after the triggers action.
--(INSERT,UPDATE and DELETE)

CREATE TABLE tblEmployee46
(
  Id int Primary Key,
  Name nvarchar(30),
  Salary int,
  Gender nvarchar(10),
  DepartmentId int
)


Insert into tblEmployee46 values (1,'John', 5000, 'Male', 3)
Insert into tblEmployee46 values (2,'Mike', 3400, 'Male', 2)
Insert into tblEmployee46 values (3,'Pam', 6000, 'Female', 1)
Insert into tblEmployee46 values (4,'Todd', 4800, 'Male', 4)  
Insert into tblEmployee46 values (5,'Sara', 3200, 'Female', 1) 
Insert into tblEmployee46 values (6,'Ben', 4800, 'Female', 3) 

CREATE TABLE tblDepartment46
(
  DeptId int identity(1,1) primary key,
  DepartmentName nvarchar(1000)
)

Insert into tblDepartment46 values ('IT')
Insert into tblDepartment46 values ('Payroll')
Insert into tblDepartment46 values ('HR')
Insert into tblDepartment46 values ('Admin')

select * from tblEmployee46 
select * from tblDepartment46

ALTER TABLE tblEmployee46 add constraint FK_tblEmployee46_DepartmentId
FOREIGN KEY (DepartmentId) references tblDepartment46(DeptId)

use[SQL-SERVER-PRAGIM-DATABASE]
GO
select * from tblEmployee46
select * from tblDepartment46


--Creating a View 
CREATE View VWEMployeeDetails46
as
select Id,Name,Gender,DepartmentName
from tblEmployee46
JOIN tblDepartment46
ON tblEmployee46.DepartmentId = tblDepartment46.DeptId

--Executing the View
select * from VWEmployeeDetails46

--Update addecting multiple base tables
update VWEmployeeDetails46 set Name='johny',DepartmentName = 'HR' where Id=1

update VWEmployeeDetails46 set DepartmentName = 'IT' where Id = 1

update VWEmployeeDetails46 set DepartmentName = 'HR' where Id = 3
---------------------------------------------------------------------------------
ALTER Trigger Tr_VWEmployeeDetails_InsertOfUpdates
ON VWEMployeeDetails46
instead of update
as
Begin
		--if EmployeeId is updated
		if(Update(Id))
		Begin
			Raiserror('Id cannot be changed',16, 1)
			Return
		End

		--if DepartmentName is updated
		if(Update(DepartmentName))
		Begin
			Declare @DepartId int

			select @DepartId = DeptId
			from tblDepartment46
			JOIN inserted
			ON inserted.DepartmentName = tblDepartment46.DepartmentName

			if(@DepartId is NULL)
			Begin
				Raiserror('Invalid Department Name',16,1)
				Return
			End
			
			Update tblEmployee46 set DepartmentId = @DepartId
				from inserted
				JOIN tblEmployee46
				ON tblEmployee46.Id = inserted.Id
			end
			
			-- if gender is updated
			if(Update (Gender))
			Begin
				Update tblEmployee46 set Gender = inserted.Gender
				from inserted
				JOIN tblEmployee46
				ON tblEmployee46.Id = inserted.Id
			End
				
			--if Name is updated
			if(Update (Name))
			Begin
				Print 'Name Changed'
				Update tblEmployee46 set Name = inserted.Name
				from inserted
				JOIN tblEmployee46
				ON tblEmployee46.Id = inserted.Id
			End
End
 
 
update tblDepartment46 set DepartmentName = 'HR' where DeptId = 1

Update VWEMployeeDetails46 set DepartmentName = 'sjd2ioklsd' where Id = 1

Update VWEMployeeDetails46 set DepartmentName = 'IT' where Id = 1

update VWEMployeeDetails46 set Name = 'Johny', Gender = 'Female', DepartmentName = 'Payroll'
where Id = 1

update VWEMployeeDetails46 set Name = 'Johny' where Id = 1

select * from VWEmployeeDetails46

