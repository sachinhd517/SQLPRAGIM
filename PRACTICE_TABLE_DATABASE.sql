use [Tables-Database]

select * from employees

select			employees.EMPLOYEE_ID as Employee, employees.DEPARTMENT_ID as Manager
from			employees E
CROSS JOIN		employees D

SELECT			E.EMPLOYEE_ID as Employee, M.MANAGER_ID as Manager
FROM			employees E
LEFT JOIN		employees M
ON				E.EMPLOYEE_ID = M.MANAGER_ID

select			E.FIRST_NAME as Employee, M.FIRST_NAME as Manager
from			employees E
LEFT JOIN		employees M
ON				E.EMPLOYEE_ID = M.MANAGER_ID

select			E.FIRST_NAME as Employee, CASE WHEN M.FIRST_NAME IS NULL THEN 'No Manager' ELSE M.FIRST_NAME END as Manager
from			employees E
LEFT JOIN		employees M
ON				E.EMPLOYEE_ID = M.MANAGER_ID

select * from employees

select EMPLOYEE_ID,FIRST_NAME,MANAGER_ID from employees

select COUNT(*) from employees where SALARY > 5000 AND SALARY < 10000 

SELECT ISNULL(NULL,'No Manager') as Manager

select DEPARTMENT_ID,MANAGER_ID  from employees
UNION ALL
select DEPARTMENT_ID,MANAGER_ID from DEPARTMENTS


select DEPARTMENT_ID,MANAGER_ID  from employees
UNION
select DEPARTMENT_ID,MANAGER_ID from DEPARTMENTS


select DEPARTMENT_ID,MANAGER_ID, FIRST_NAME from employees

select			E.FIRST_NAME as Emp, COALESCE(NULL, 'No Manager') as Dept
from			employees E
LEFT JOIN		employees M
ON				E.DEPARTMENT_ID = M.MANAGER_ID


select			E.FIRST_NAME as Employee, CASE WHEN M.FIRST_NAME IS NULL THEN 'No Manager' ELSE M.FIRST_NAME END as Manager 
from			employees E
LEFT JOIN		employees M
ON				E.EMPLOYEE_ID = M.MANAGER_ID


select			E.FIRST_NAME as Employee, M.FIRST_NAME as Manager
from			employees E
LEFT JOIN		employees M
ON				E.EMPLOYEE_ID = M.MANAGER_ID


select EMPLOYEE_ID,FIRST_NAME,MANAGER_ID from employees

Declare @Number int
set 	@Number = 65
While(@Number <= 255)
BEGIN
	Print CHAR(@Number)
	set	@Number = @Number + 1
END 

Declare @Start int
Set @Start = 123
While(@Start <= 150)
Begin 
	Print CHAR(@Start)
	Set @Start = @Start + 1
End


select SQUARE(12.23)
select SQRT(12.00)
select GETDATE()
select DAY('2012/1/12')

select * from tbl_EmployeePart14


CREATE FUNCTION MYFUNCTION(@DEPARTMENT_NAME nvarchar(50))
RETURNS TABLE
AS 
RETURN (select FIRST_NAME, LAST_NAME,EMAIL
		from employees
		where  DEPARTMENT_NAME = @DEPARTMENT_NAME)



CREATE FUNCTION INLINE_EmployeeBySalary(@EmployeeID int)
RETURNS TABLE
as
RETURN (select FIRST_NAME,LAST_NAME,SALARY
		from employees
		where EMPLOYEE_ID = @EmployeeID)

select * from [INLINE_EmployeeBySalary](10)


CREATE INDEX IX_employee_salary
ON employees (salary DESC)


execute index IX_employee_salary


create clustered index IX_employees_Gender_Salary
ON employees (PHONE_NUMBER DESC,SALARY ASC)

create nonclustered index IX_NonEmployees_Gender_Salary
ON employees (PHONE_NUMBER DESC, SALARY ASC)

Alter table employees
add constraint UQ_employees_PhoneNumber
UNIQUE (PHONE_NUMBER)

select		E.FIRST_NAME as Employees,	M.FIRST_NAME as Manager
from		employees E
LEFT JOIN	employees M
ON			E.EMPLOYEE_ID = M.MANAGER_ID

select		E.FIRST_NAME as Employees, M.FIRST_NAME ISNULL NULL, 'No Manager' as Manager
FROM		employees E
LEFT JOIN	employees M
ON			E.EMPLOYEE_ID = M.MANAGER_ID

CREATE INDEX IX_employee_Salary
ON employees(SALARY asc)

CREATE INDEX IX_employees_Salary
on employees(PHONE_NUMBER desc,SALARY ASC)


CREATE TRIGGER Tr_employees_ForInsert
ON employees
FOR INSERT
AS
BEGIN
		Declare @ID int
		select @ID = EMPLOYEE_ID from inserted
		insert into employees values('Employee added with ID = '+ CAST(@ID as int) + 'the time is ' + CAST(GETDATE() as varchar(100)))
END