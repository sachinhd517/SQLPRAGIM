--What is Normalization

--Database  normalization is the process fo organizing data to minimize data redundancy (data duplications),
--which in turn ensures data consistency.

--Problem of Data Redundancy
--1.	Disk space watage
--2.  Data Inconsistency
--3.  DML queries can become slow

--Datebase normalizations is a step by step process. There are 6 normal forms, first
--Normal form(1NF) thru sixth Normal Form(6NF). Most database are in thired normal from(3NF).
--There are certain rules, that reach normal form should follow.

--A table is said to be in 1NF, if
--1.	The data in each column shuld be atomic. No multiple values, separated by comma.
--2.	The table does not contiain nay repeating column groups
--3.	Identify each record uniquely using primary key.


