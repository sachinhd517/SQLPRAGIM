--View Limitation in sql server 

--VIEW Limmitations

--1.	You cannot pass parameters to a view. Table
--valued functions are an excellent replacement for
--parameterized views.

--2.	Rules and Defaults cannot be associated with views.

--3.	The ORDER BY clause is invalid in views unless
--Top or FOR XML is also specified.


create table tblEmployees42
(
	[ID] int Primary key NOT NULL ,
	[Name] nvarchar(50) NOT NULL,
	[Gender] nvarchar(50) NOT  NULL,
	[Salary] int,
	[DepartmentId] int
)

select * from tblEmployees42
select * from tblDepartments42

truncate table tblEmployees42  


insert into tblEmployees42 values(1,'Tom','Male',4000,1)
insert into tblEmployees42 values(2,'Pam','Female',3000,3)
insert into tblEmployees42 values(3,'John','Male',3500,1)
insert into tblEmployees42 values(4,'Sam','Male',4500,2)
insert into tblEmployees42 values(5,'Todd','Male',2800,2)
insert into tblEmployees42 values(6,'Ben','Male',7000,1)
insert into tblEmployees42 values(7,'Sara','Female',4800,3)
insert into tblEmployees42 values(8,'Valaire','Female',5500,1)
insert into tblEmployees42 values(9,'James','Male',6500,null)
insert into tblEmployees42 values(10,'Russell','Male',8800,NULL)

Create table tblDepartments42
(
	[Id] int primary key NOT NULL,
	[DepartmentName] nvarchar(50) NOT NULL,
	[Location] nvarchar(50) NOT NULL,
	[DepartmentHead] nvarchar(50)
)

select * from tblDepartments42

Insert into tblDepartments42 values (1, 'IT', 'London', 'Rick')
Insert into tblDepartments42 values (2, 'Payroll', 'Delhi', 'Ron')
Insert into tblDepartments42 values (3, 'HR', 'New York', 'Christie')
Insert into tblDepartments42 values (4, 'Other Department', 'Sydney', 'Cindrella')

alter table tblEmployees42 add constraint FK_tblEmployees42_DepartmentId
FOREIGN KEY (DepartmentId) references tblDepartments42(Id)


Create view VWEmployeeDetails
as
select ID, Name, Gender, DepartmentID
from tblEmployees42

select * from VWEmployeeDetails


--Error : cannot pass Parameters to Views
-----------------------------------------

Create View VWEmployeeDetails
@Gender nvarchar(20)
as
select ID, Name, Gender, DepartmentID
from tblEmployees42
where Gender = @Gender

--You can filter Gender in the where clause
select * from VWEmployeeDetails where Gender = 'Male'

-- Inline Table valued function as  a repalcement for
-- Parameterized views.

Create function fnEmployeeDetails(@Gender nvarchar(20))
Returns Table
as
Return 
	(Select ID, Name, Gender, DepartmentID 
	from tblEmployees42 
	where Gender = @Gender)

select * from dbo.fnEmployeeDetails('Male')

Create Table ##TestTempTable42( Id int , [Name] nvarchar(20), Gender nvarchar(10))

insert into ##TestTempTable42 values(101,'Martin','Male')
insert into ##TestTempTable42 values(102,'Joe','Female')
insert into ##TestTempTable42 values(103,'Pam','Female')
insert into ##TestTempTable42 values(104,'Jamew','Male')

select * from ##TestTempTable42

Create View VWONTempTable42
as
select Id, Name, Gender 
from ##TestTempTable42

