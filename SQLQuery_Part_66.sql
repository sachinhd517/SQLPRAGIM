USE[SQLSERVERPRAGIM]
CREATE TABLE tblEmployee66
(
    ID INT IDENTITY NOT NULL PRIMARY KEY,
    Name NVARCHAR(100),
    Gender NVARCHAR(100),
    DateOfBirth DATETIME
)

SELECT * FROM tblEmployee66

USE[SQLSERVERPRAGIM]
IF not exists (SELECT * FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'tblEmployee66')
BEGIN
    CREATE TABLE tblEmployee66
    (
        ID INT IDENTITY NOT NULL PRIMARY KEY,
        Name NVARCHAR(100),
        Gender NVARCHAR(100),
        DateOfBirth DATETIME
    )
    PRINT 'Table tblEmployee66 successfully created'
END
ELSE
BEGIN
    PRINT 'Table tblEmployee66 already exists'
END

select OBJECT_ID('tblEmployee66')


IF OBJECT_ID('tblEmployee66') IS NULL
BEGIN
    PRINT 'Table tblEmployee created'
END
ELSE
BEGIN
    PRINT 'Table tblEmployee66 already exists'
END

IF OBJECT_ID('tblEmployee66') IS NOT NULL
BEGIN
    DROP TABLE tblEmployee66
END
CREATE TABLE tblEmployee66
    (
        ID INT IDENTITY NOT NULL PRIMARY KEY,
        Name NVARCHAR(100),
        Gender NVARCHAR(100),
        DateOfBirth DATETIME
    )


IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'EmailAddress'
AND TABLE_NAME = 'tblEmployee66' AND TABLE_SCHEMA = 'dbo')
BEGIN
    ALTER TABLE tblEmployee66
    ADD EmailAddress NVARCHAR(50)
END
ELSE
BEGIN
    PRINT 'Column EmailAddress already exists'
END