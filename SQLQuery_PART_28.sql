--CAST AND CONVERT FUNCTION IN SQL SERVER PART - 28

-- TO Convert one data type to another, CAST and CONVERT function can be used.

--CAST (expression AS date_type [(length)])
--CONVERT (date_type[length]),expression [, style])

--Difference Between CAST and CONVERT

--1. Cast is bases on ANSI standered and covert is specific to sql server. So, if portability is a 
--concern and if you want to use the script with other database application use CAST()

--2. Convert provides more flexibility than Cast. FOr example, it's possible to control how you want
--Datetime database to be converted using sytle with convert function.

use[ASP_DATABASE]
GO

select * from tblDateofBirthEmp

-- CAST  CONVERT FUNCTION

 select ID, Name, DateOfBirth, CAST(DateOfBirth as nvarchar(10)) as ConvertedDOB from tblDateofBirthEmp

 select ID, Name, DateOfBirth, CONVERT (nvarchar,DateOfBirth, 101) as ConvertedDOB from tblDateofBirthEmp

 select ID, Name, Name + ' - ' +CAST(ID as nvarchar)as [Name - ID] from tblDateofBirthEmp
  use[ASP_DATABASE]
  GO

  select * From tblEmployeeRegistration

  select RegistrationDate, count(ID) as Total
  from	 tblEmployeeRegistration
  Group by RegistrationDate

  
  select CAST(RegistrationDate as Date) as RegistrationDate , count(ID) as Total
  from	 tblEmployeeRegistration
  Group by CAST(RegistrationDate as Date)