-- REPLICATE,SPACE,PATINDEX<REPLACE AND STUFF STRING FUNCTION IN SQL SERVER PART - 24

CREATE TABLE tblPersones
(
	[FirstName] nvarchar(50) NOT NULL,
	[LastName] nvarchar(50) NOT NULL,
	[Email] nvarchar(50) NOT NULL
)

insert into tblPersones values('Sam','Sony','Sam@aaa.com')
insert into tblPersones values('Ram','Barber','Ram@aaa.com')
insert into tblPersones values('Sara','Sanosky','Sara@ccc.com')
insert into tblPersones values('Todd','Garthner','Todd@bbb.com')
insert into tblPersones values('john','Grover','John@aaa.com')
insert into tblPersones values('Sana','Lenin','Sana@ccc.com')
insert into tblPersones values('James','Bond','James@bbb.com')
insert into tblPersones values('Rob','Hunter','Rob@ccc.com')
insert into tblPersones values('Steve','Wilson','Steave@aaa.com')
insert into tblPersones values('Pam','Broker','Pam@bbb.com')


select * from tblPersones

-- REPLICATE FUNCTION

SELECT REPLICATE('sachin',    4) as Name


select FirstName, LastName,
		SUBSTRING(Email,  1,  2) + REPLICATE('*',5) + 
		SUBSTRING(Email, CHARINDEX('@',Email),LEN(Email) - CHARINDEX('@',Email)+1) as Email
from tblPersones

-- SPACE FUNCTION

select SPACE(9)as Email

select FirstName + SPACE(9) + LastName as FullName
FROM tblPersones

--PATINDEX FUNCTION

--PATINDEX('%Pattern%',Expression)

--Return the starting position of the first occureance of a pattern in a specified expression. It
--takes two arguments the pattern to be searched and the expression. PATINDEX() is simial to
--CHARINDEX(). With CHARINDEX() we cannot use wildcards, where as PATINDEX() provides this
--capability. It the specified patten is not found, PATINDEX() returns ZERO.


select Email, PATINDEX('%@aaa.com',Email) as Firstoccerences
from tblPersones
where PATINDEX('%@aaa.com',Email) > 0


--REPLACE FUNCTION

--REPLACE(String_Expression,pattern,Replacements_Value)

--Replaces all occurences of a specified string value with another string value.

select Email, REPLACE(Email,'.com','.net') as ConvertedEmail
from tblPersones

--STUFF FUNCTION

--STUFF(Original_Expression, Start, Length, Palcements_expression)
--STUFF()function inserts Pelacements_expression, at the start position specfied, along with
--removing the charractes specified usingLength parameter.

select FirstName,LastName,Email,
STUFF(Email,  2,  3,  '*****') as StuffedEmail
from tblPersones