--IMPORTANT CONCEPTRELATED TO FUNCTION IN SQL SERVER PART - 33 

--In this seesion we will learn

--1.	Deterministic and Nondeterministic Fucntions
--2.	Encrypting functions
--3.	Schema binding functions

 --Deterministic AND Nondeterministic
 

 --Deterministic function alsways return the same result any time they are called with a
 --sepecific set of input values and given the same state of the database.
 
 --Example : Square(),Power(),Sum(),AVG() and COUNT()

 --Note : All aggregate function are dterministic functions.

 --Nondeteministic function may return different results each time they are called with a 
 --specific set of input values even if the database state that they access remain the
 --same.

 --Example : GetDate() and CURRENT_TIMESTAMP

 --Rand() fucntion is a Non-deterministic function, but if you provide the seed value, the
 --function becomes dterministic, as the same value returned for the same seed
 --value.

 
create table tblEmployee33
(
	[Id] int primary key NOT NULL,
	[Name] nvarchar(50) NOT NULL,
	[Gender] nvarchar(50) NOT NULL,
	[DateOfBirth] Date NULL,
	[DepartmentId] int NOT NULL
)
Create table tblDepartment33
(
	[ID] int primary key,
	[Department_Name] nvarchar(50)
)

Alter table tblEmployee32 add DateOfBirth Date

select * from tblEmployee33
select * from tblDepartment33


Alter table tblEmployee33 add constraint FK_tblEmployee33_GenderId
FOREIGN KEY (DepartmentId) references tblDepartment33(ID)

insert into tblEmployee33 values(1,'sam','Male',NULL,1)
insert into tblEmployee33 values(2,'Ram','Male',NULL,1)
insert into tblEmployee33 values(3,'Sara','Female',NULL,3)
insert into tblEmployee33 values(4,'Todd','Male',NULL,2)
insert into tblEmployee33 values(5,'John','Male',NULL,3)



insert into tblDepartment33 values(1,'Devloper')
insert into tblDepartment33 values(2,'HR')
insert into tblDepartment33 values(3,'Account')


 select * from tblEmployee33

 select COUNT(*) from tblEmployee33

 select SQUARE(3)


 select GETDATE()

 select CURRENT_TIMESTAMP

SELECT RAND(1)
SELECT RAND()

select RAND()

--WITH ENCRTPTION AND SCHEMABINDING

--Encrypting a function defintion using WITH ENCRYPTION OPTION:

--	Wehave learnt how to encrypt Stored procedure text using WITH ENCRYPTION OPTION in
--	Part 18 of this video series. Along the same lines, you can also encrypt a function text.
--	Once, encrypted, you cannot view the text of the function, using sp_helptext system
--	stored procedure. If you try to, you will get a message starting 'The text for object is
--	encrypted.' There are ways to decrypt, which is beyound the scope of this video.

--	Use WITHENCRYPTION

--	Creating a function WITH SCHEMABINDINF option :

--	Schemabinding, specifies that the function is bound to the database object that it
--	references. When SCHEMABINDING is specified, the base objects, the base objects cannot be modified in
--	any way that would affect the function definition. The function definition iteself must first
--	be modified or dropped to remove depedencies on the object that is to be modified.

--	Use WITHSCHEMABINDING
 
 select * from tblEmployee33

 CREATE FUNCTION fn_GetNameById(@Id int)
 RETURNS nvarchar(30)
 as
 Begin
		return (select Name from tblEmployee33 where ID = @Id)

 END

 select dbo.fn_GetNameById(1)

 sp_helptext fn_GetNameById

 ALTER FUNCTION fn_GetNameById(@Id int)  
 RETURNS nvarchar(30)  
 WITH ENCRYPTION
 as  
 Begin  
  return (select Name from tblEmployee33 where ID = @Id)  
  
 END

 sp_helptext fn_GetNameById

 ----------------------------------------------------------------------------

 --How to remove encryption?

 --1. remove tag Encryption
 --2. Alter function 
	--Ex : ALTER FUNCTION (FUNCTION_NAME)

ALTER FUNCTION fn_GetNameById(@Id int)  
RETURNS nvarchar(30)  
 
 as  
 Begin  
  return (select Name from tblEmployeThirtyOneTable where ID = @Id)  
  
 END


  sp_helptext fn_GetNameById

  --------------------------------------------------------------------
-- WITH  SCHEMABINDING EXAMPLE

--1. Any user can't deleted table when using SCHEMABINDING

 --For Example :

 ALTER FUNCTION fn_GetNameById(@Id int)  
 RETURNS nvarchar(30)  
 WITH SCHEMABINDING
 as  
 Begin  
  return (select Name from dbo.tblEmployee33 where ID = @Id)  
  
 END

 drop table tblEmployee33



