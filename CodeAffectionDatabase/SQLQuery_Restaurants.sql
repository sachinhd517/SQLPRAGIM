insert into Customer values('N Olivia Kathleen')
insert into Customer values('N Lian Patrick')
insert into Customer values('N Charlotte Rose')
insert into Customer values('N Elijah Burke')
insert into Customer values('N Ayesha Ameer')
insert into Customer values('N Eve Louis')

select * from Customer
select * from Item

insert into Item values('Chicken Tenders',3.50)
insert into Item values('Chicken Tenders w/ Fries',4.99)
insert into Item values('Chicken Tenders w/ Onion',5.99)
insert into Item values('Grilled Cheese Sandwich',2.50)
insert into Item values('Grilled Cheese Sandwich w/ Fries',3.99)
insert into Item values('Grilled Cheese Sandwich w/ Onion',4.99)
insert into Item values('Lettuce and Tomato Burger', 1.99)
insert into Item values('Soup',2.50)
insert into Item values('Onion Rings',2.99)
insert into Item values('Fries',1.99)
insert into Item values('Sweet Potato Fries',2.49)
insert into Item values('Sweet Tea',1.79)
insert into Item values('Bottle Water',1.00)
insert into Item values('Canned Drinks',1.00)