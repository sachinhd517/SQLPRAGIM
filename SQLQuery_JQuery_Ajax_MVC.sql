Create table tblEmployees
(
	[EmployeeId] int primary key Identity,
	[Name] varchar(50),
	[Position] varchar(50),
	[Office] varchar(50),
	[Salary] int,
	[ImagePath] varchar(500)
)

select * from tblEmployees