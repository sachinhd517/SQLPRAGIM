--ERROR Handling in sql server Part 56

--With the introduction of Try/Catch blocks in SQL Server 2005, error handling in sql 
--server, is no similar to programming languages like C#, and java.

--Error Handling in SQL Server 2000- @@Error
--Error Handling in SQL Server 2005 & later- Try....Catch

--Note- Sometimes, system functions that begin with two at singns(@@), are called as
--gobla variables. Thye are not variables and do not have the same behavious as 
--variabels, instead they are very similar to functions.

--RAISERROR('Error Message',ErrorSeverity,ErrorState)
--Create and retrun custom errors
--Severity level = 16(indicate general errors that can be corrected by the user)
--State = Numbers between 1 to 255. RAISERROR only generates errors with state 
--from 1 through 127.


--Any set of SQL statements, that can possibly throw an execption are wrapped between 
--BEGIN TRY and END TRY blocks. If there is an exception in the TRY block, the control
--immediately, jumps to the CATCH block. If there is no exception, CATCH blcok will be
--skipped, and the stements, after the CATCH block are executed.

--Errors trapped by a CATCH block are not returned to the calling application. IF any part  of 
--the error information must be returned to the application, the code in thCATCH blcok
--must do so by using RAISEEOR() functino.

--In the scope of the CATCH block, there are several system functions, that are used to 
--retrieve more information about the error that occurred. These function return NULL if
--they are executed outside the scope of the CATCH block. TRY/CATCH cannot be used
--in a suer-defined functions.


Create Table tblProduct56
(
 ProductId int NOT NULL primary key,
 Name nvarchar(50),
 UnitPrice int,
 QtyAvailable int
)

Create Table tblProductSales56
(
 ProductSalesId int primary key,
 ProductId int,
 QuantitySold int
) 

select * from tblProduct56
select * from tblProductSales56

Insert into tblProduct56 values(1, 'Laptops', 2340, 100)
Insert into tblProduct56 values(2, 'Desktops', 3467, 50)

ALter table tblProduct56 add constraint FK_tblProduct56_ProductId
FOREIGN KEY (ProductId) references tblProduct56(ProductId)

EXEC spSellProduct56 1, 10

ALTER Procedure spSellProduct56
@ProductId int,
@QuantityToSell int
as
Begin
	--Check the strock avaiable, for the rpducts we want to sell

	Declare @StockAvaiable int
	Select @StockAvaiable = QtyAvailable
	from tblProduct56 where ProductId = @ProductId

	--Throw an error to the calling application, of enough stock
	--is not available

	if(@StockAvaiable < @QuantityToSell)
		Begin
			Raiserror('Not enough stock avaiable', 16,1)
		End

	--If enough stock available

	Else
		Begin Try
			Begin Transaction
				--First reduce the qiantity available
				Update tblProduct56 set QtyAvailable = (QtyAvailable - @QuantityToSell)
				where ProductId = @ProductId

				Declare @MaxProductSalesId int
				--Calculate MAX ProductDalesId
				select @MaxProductSalesId = CASE When	
					MAX(ProductSalesId) IS NULL
					THEN 0 ELSE MAX(ProductSalesId) end
					FROM tblProductSales56

				-- Increment @MaxProductSalesId by 1, so we don't get a primary key violation

				--SET @MaxProductSalesId = @MaxProductSalesId + 1
				INSERT INTO tblProductSales56 VALUES(@MaxProductSalesId, @ProductId,@QuantityToSell)
				COMMIT TRANSACTION
			END Try
			Begin Catch
				Rollback Transaction
				Select
					ERROR_NUMBER() as ErrorNumber,
					ERROR_MESSAGE() as ErrorMessage,
					ERROR_PROCEDURE() as ErrorProcedure,
					ERROR_STATE() as ErrorState,
					ERROR_SEVERITY() as ErrorSeverity,
					ERROR_LINE() as ErrorLine
				END CATCH
				
		END
