-- DateTime Data Types in SQL SERVER Part - 26

--GETDATE()				:	Commonly used
--CURRENTT_TIMESTAMP	:	ANSI SQL equlvalent to GETDATE
--SYSDATETIME()			:	More fractional seconds precision
--SYSDATETIMEOFFSET()	:	More fractional seconds precision + Time zone offset
--GETUTCDATE()			:	UTC Date and Time
--SYSUTCDATETIME()		:	UTC Date and Time, with More fractional seconds precision



select ISDATE('sachin')

select ISDATE(GETDATE())

select ISDATE(CURRENT_TIMESTAMP)

-- DAY,MONTH,YEAR FUNCTION 

select DAY(GETDATE())

select MONTH(GETDATE())

select YEAR(GETDATE())

-- DATENAME FUNCTION

--DateName(DatePart, Date) - Returns a string, that represents a part of the given date. This
--function takes 2 parameters. The First parameter Datepart sepecifies, the part of the date, we
--want. The second paramter, is the actual date, from which we want the part of the Date.


	select SYSDATETIME()
	select SYSDATETIMEOFFSET()
	select GETDATE()
