select * from tblEmployees

select SYSDATETIME() as Today_Date

use [GRIDVIEW_TUTORIAL]
select * from tblEmployees

sp_help tblEmployees

Create table tbl_Emp
(
	[EmployeeID] int primary key NOT NULL,
	[FirstName] nvarchar(50) NOT NULL,
	[AnnualSalary] int NOT NULL,

	[Country] nvarchar(50) NOT NULL
)

insert into tbl_Emp values(1,'John',55000,'US')
insert into tbl_Emp values(2,'Pam',62000,'India')
insert into tbl_Emp values(3,'David',45000,'UK')
insert into tbl_Emp values(4,'Rose',78000,'South Africa')
insert into tbl_Emp values(5,'Mark',69000,'Malaysia')

select * from tbl_Emp

