--PART 8

--PreReq-Part 7 - Identity Column in SQL Server

--From the previous session, we understood that identity column values are auto
--generated. There are several wasy in sql server, to retrieve the last identity value that is
--generated. The most common way is to use SCOPE_IDENTITY() build in function.

--Note : You can also use @@IDENTITY and IDENT_CURRENT('TableName')

--Difference : 

--SCOPE_IDENTITY() - Same Session and same scope.
--@@IDENTITY - Same Session and across any scope.
--IDENT_CURRENT('TableName') - Specific table across any session and any scope.

Create table Test1
(
	[ID] int identity(1,1),
	[Value] nvarchar(50)
)
Create table Test2
(
	[ID] int identity(1,1),
	[Value] nvarchar(50)
)
select * from Test1
select * from Test2


insert into Test1 values('s')

select SCOPE_IDENTITY()
select @@IDENTITY
select IDENT_CURRENT('Table2')

Create Trigger TrForInset on Test1 for Insert
as
Begin
	insert into Test1 values ('qqqq')
End


ALTER TABLE tblPerson add constraint tblPerson_GenderID_FK
Foreign key (GenderID) references tblGender(ID)

select * from tblGender
select * from tblPerson

insert into tblPerson(Name,Email) values('Ram','Ram@.gmail.com')
 
ALTER TABLE tblPerson ADD CONSTRAINT DF_tblPerson_GenderID DEFAULT 3 for GenderID

insert into tblPerson(Name,Email) values ('Shyam','Shyam123@gmail.com')

ALTER TABLE tblPerson DROP CONSTRAINT DF_tblPerson_GenderID

ALTER TABLE tblPerson ADD CONSTRAINT DF_tblPerson_GenderID DEFAULT 3 FOR GenderID

ALTER TABLE tblPerson DROP CONSTRAINT DF_tblPerson_GenderID

DELETE FROM tblGender where ID = 3

ALTER TABLE tblPerson ADD CONSTRAINT tblPerson_GenderID_FK
FOREIGN KEY (GenderID) references tblGender(ID)

ALTER TABLE tblGender ADD CONSTRAINT tblPerson_GenderID_FK
FOREIGN KEY (GenderID) references tblGender(ID)








