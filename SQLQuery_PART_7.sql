

--IF a column is marked as an identity column, then the value for this column are
--automatcially generated, when you insert a new into table,.

--Create Table {Table_Name}
--(
--	PersonID int identity(1,1) primary key,
--	Name nvarchar(50)	
--)

--Note : Seed and increment values are optional. If you don't sepcify the identity and seed they both default to 1

--To explicitly supply a value for identity column
--1.	First turn on identity insert - SET identity_Insert tblPerson ON
--2.	In the insert query sepcify the column list
--	insert into tblPerson2(PersonID,Name) values(2,'John')

--if you have deleted all the rows in a table, and you want to reset the identity column 
--value, use DBCC CHECKIDENT('tblPerson',RESEED,0)

CREATE TABLE tblPerson7
(
	[PerID] int primary key NOT NULL,
	[PersonName] nvarchar(50) NOT NULL
)

use [SQLSERVERPRAGIM]
GO
insert into tblPerson7 values(1,'Sachin')
insert into tblPerson7 values(2,'Harish')
insert into tblPerson7 values(3,'Rajesh')
insert into tblPerson7 values(4,'Akshay')
			
delete from tblPerson7 where PerID = 4

TRUNCATE table  tblPerson7

select * from tblPerson7

--HOW  SET OFF IDENTITY

set IDENTITY_INSERT tblPerson7 OFF

DBCC CHECKIDENT(tbl_Person2, RESEED, 0)