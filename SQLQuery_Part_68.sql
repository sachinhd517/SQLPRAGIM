USE[SQLSERVERPRAGIM]
GO

CREATE TABLE tblEmployee68
(
    Id int PRIMARY KEY NOT NULL IDENTITY,
    Name NVARCHAR(50),
    Email NVARCHAR(50),
    Age INT,
    Gender NVARCHAR(50),
    HireDate DATE
)

Insert into tblEmployee68 values
('Sara Nan','Sara.Nan@test.com',35,'Female','1999-04-04')
Insert into tblEmployee68 values
('James Histo','James.Histo@test.com',33,'Male','2008-07-13')
Insert into tblEmployee68 values
('Mary Jane','Mary.Jane@test.com',28,'Female','2005-11-11')
Insert into tblEmployee68 values
('Paul Sensit','Paul.Sensit@test.com',29,'Male','2007-10-23')

SELECT * FROM tblEmployee68

CREATE PROCEDURE spSearchEmployees
@Name NVARCHAR(50) = NULL,
@Email NVARCHAR(50) = NULL,
@Gender NVARCHAR(50) = NULL,
@Age INT = NULL
AS
BEGIN
        SELECT * FROM tblEmployee68 WHERE
        @Name = Name AND @Email= Email AND 
        @Gender = Gender AND @Age = Age
END

ALTER PROCEDURE spSearchEmployees
@Name NVARCHAR(50) = NULL,
@Email NVARCHAR(50) = NULL,
@Gender NVARCHAR(50) = NULL,
@Age INT = NULL
AS
BEGIN
        SELECT * FROM tblEmployee68 WHERE
        (@Name = Name OR @Name IS NULL) AND 
		(@Email= Email OR @Email IS NULL) AND 
        (@Gender = Gender OR @Gender IS NULL) AND 
		(@Age = Age OR @Age IS NULL)
END

EXECUTE spSearchEmployees

EXECUTE spSearchEmployees @Gender = 'Male'

EXECUTE spSearchEmployees @Gender = 'Male', @Age = 29
