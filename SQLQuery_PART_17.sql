-- UNION AND UNION ALL IN SQL SERVER PART 17

--UNION & UNION ALL JOIN

--DIfference between UNION AND UNION ALL
--1.	UNION removes duplicate rows, where as UNION ALL does not
--2.	UNION has to perform distinct sort to remoeve duplicates, which makes it less faster then UNION ALL

--Note:  Estimated query execution plan- CTRL + L

--Sorting result of a UNION or UNIONALL

--ORDER BY clause should be used only on the last SECLECT statment in the UNION query

--Difference between UNION and JOIN

--UNION comnbine the result set to two or more select queries into a single result set which
--includes all the rows from all the queries in the union, where as JOIN, retrieve data form two or 
--more tables based on logical relationships between the tables.

--In short, UNION combines rows from 2 or more tables, where JOINS combine columns from 2 or
--more table.

CREATE TABLE tblIndianCustomer
(
	[ID] int NOT NULL IDENTITY,
	[Name] nvarchar(50) NOT NULL,
	[Email] nvarchar(50) NOT NULL 
)

CREATE TABLE tblUKCustomer
(
	[ID] int NOT NULL IDENTITY,
	[Name] nvarchar(50) NOT NULL,
	[Email] nvarchar(50) NOT NULL
)


insert into tblIndianCustomer values('Raj','r@r.com')
insert into tblIndianCustomer values('sam','s@s.com')

insert into tblUKCustomer    values('Ban','B@B.com')
insert into tblUKCustomer values('sam','s@s.com')
------------------------------------------------------

--  UNION AND UNION ALL EXAMPLES

select * from tblIndianCustomer
UNION 
select * from tblUKCustomer

select * from tblIndianCustomer
UNION ALL 
select * from tblUKCustomer
-----------------------------------------------------
-- DISPLAY ESTIMATE EXECUTION PLAN (CTRL + L)

select * from tblIndianCustomer
UNION 
select * from tblUKCustomer


select * from tblIndianCustomer
UNION ALL
select * from tblUKCustomer

----------------------------------------------------
-- IT'S WRONG 
select ID, Name, Email from tblIndianCustomer
ORDER BY NAME
UNION 
select * from tblUKCustomer

select ID, Name, Eamil from tblIndianCustomer
UNION 
select ID, Name from tblUKCustomer
---------------------------------------------------
--IT'S CORRECT 

select ID,Name,Email from tblIndianCustomer
UNION 
select ID,Name,Email from tblUKCustomer

select ID,Name,Email from tblIndianCustomer
UNION ALL 
select ID,Name,Email from tblUKCustomer
----------------------------------------------------

select ID,Name,Email from tblIndianCustomer
UNION ALL

select ID,Name,Email from tblUKCustomer
UNION ALL

select ID,Name,Email from tblIndianCustomer
ORDER BY Name