--CHECEK CONSTRAINT

--CHECEK constraint is used to limit the range of the values, that can be entered for a column.

--The general formula for adding check constriant in SQL Server : 

--ALTER TABLE {TABLE_NAME}
--ADD CONSTRAINT {CONSTRAINT_NAME}CHECK{BOOLEAN_EXPRESSION}

--If the BOOLEAN_EXPRESSION returns true, then the CHECK coonstraint allows the value,
--otherwise it doesn't since, AGE is a nullable column, it's possible to pass null for this
--column, When inserting a row. When you pass NULL for the AGE column, the boolean
--expression evaluates to UNKNOEN, and allows the value.

--To DROP THE CHECK CONSTRAINT

--ALTER TABLE tblPerson
--DROP CONSTRAINT CK_tblPerson_Age



CREATE TABLE tblPerson6
(
	[ID] int primary key NOT NULL,
	[Name] nvarchar(50),
	[Email] nvarchar(50),
	[GenderID] int ,
	[Age] int not NULL
)

CREATE TABLE tblGender6
(
	[ID] int primary key NOT NULL,
	[Gender] nvarchar(50)
)

alter tblPerson6
modify ID PRIMARY KEY 



select * from tblGender6
select * from tblPerson6

ALTER TABLE tblPerson6 add constraint FK_tblPerson6_GenderID
FOREIGN KEY (GenderID) references tblGender6(ID)

ALTER table tblPerson1
DROP constraint CK_tblPerson1,CK_tblPerson1_1



alter table tblPerson1 
add Age int NOT NULL

insert into tblPerson6 values(1,'ram','ram123@gmail.com',1,24)
insert into tblPerson6 values(2,'shyam','shyam123@gmail.com',1,25)
insert into tblPerson6 values(3,'Sita','sita13@gmail.com',2,44)
insert into tblPerson6 values(4,'Radhika','radhika3@gmail.com',2,34)
insert into tblPerson6 values(5,'Mahesh','mahesh12@gmail.com',1,26)
insert into tblPerson6 values(6,'Rolando','roalan123@gmail.com',1,34)
insert into tblPerson6 values(7,'Jayesh','jayesh123@gmail.com',1,24)
insert into tblPerson6 values(8,'shimroz','shmroz@gmail.com',1,29)
insert into tblPerson6 values(9,'Dhanshri','Dhanshri13@gmail.com',2,30)
insert into tblPerson6 values(10,'Ram','Ram@gmail.com',1,-131)

delete  tblPerson6 where ID  = 10

insert into tblGender6 values (1,'Male')
insert into tblGender6 values (2,'Female')
insert into tblGender6 values (3,'Unknown')

--CHECK CONSTRAINT

select * from tblPerson6

Alter Table tblPerson6
add constraint CK_tblPerson6_Age CHECK (Age > 0 AND Age < 40)

ALTER TABLE tblPerson6
add constraint CK_tablePerson6_Age CHECK (Age > 0 AND Age < 40)

ALTER TABLE tblPerson6 ADD CONSTRAINT 
CK_tablePerson6_Age CHECK (Age > 0 AND Age < 40)

ALTER TABLE tblPerson6 ADD CONSTRAINT CK_tablePerson6_Age CHECK (Age > 0 AND Age < 40)

ALTER TABLE tblPerson6 ADD CONSTRAINT CK_tableperson6_Age CHECK (Age > 0 AND Age < 40)




































