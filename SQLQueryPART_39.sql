-- View in SQL SERVER PART - 39

--	What is a VIEW ?
--		A View is nothing more than a saved SQL query. A view can also be considered as a
--		Virtual table

Create Table tblEmployeesView39
(
	[ID] int Primary key IDENTITY NOT NULL,
	[Name] nvarchar(50) NOT NULL,
	[Salary] nvarchar(50) NOT NULL,
	[Gender] nvarchar(50) NOT NULL,

)



select * from tblEmployeesView39
select * from tblDepartmentView39

alter table tblEmployeesView39
add DepartmentId int

sp_help tblEmployeesView39
sp_help tblDepartmentView39

Create Table tblDepartmentView39
(
	[DepartID] int PRIMARY KEY IDENTITY NOT NULL,
	[DepartName] nvarchar(50) NOT NULL 
)

alter table tblEmployeesView39 add constraint
FK_tblEmployeesView39_DepartmentId Foreign key (DepartmentId)
references tblDepartmentView39 (DepartID)


select  ID,Name,Salary,Gender,DepartName
from tblEmployeesView39
JOIN tblDepartmentView39
ON tblEmployeesView39.ID = tblDepartmentView39.DepartID

Create View VWEMployeeByDepartment
as
select ID,Name,Salary,Gender,DepartName
from tblEmployeesView39
JOIN tblDepartmentView39
ON tblEmployeesView39.ID = tblDepartmentView39.DepartID

select * from VWEMployeeByDepartment


select * from tblEmployeesView39
select * from tblDepartmentView39

select * from VWEMployeeByDepartment

sp_helptext VWEMployeeByDepartment

--Advantages of Views

--Views can be used to reduce the complexity of the database
--sechema

--VIew can be used as a mechannisum to implement rwo and 
--column level security.

--View can be used to present aggregated data and hide
--detailed data.

--To modify a view - ALTER VIEW statment
--TO DROP a view - DROP VIEWvWName

select ID,Name,Gender,DepartName
from tblEmployeesView39
JOIN tblDepartmentView39
ON tblEmployeesView39.ID = tblDepartmentView39.DepartID
where tblDepartmentView39.DepartName = 'IT'

Create View VWITEmployees
as
select ID,Name,Gender,DepartName
from tblEmployeesView39
JOIN tblDepartmentView39
ON tblEmployeesView39.ID = tblDepartmentView39.DepartID
where tblDepartmentView39.DepartName = 'IT'

select * from VWITEmployees

Create View VWNonConfidentionalData
as
select ID,Name,DepartName
from tblEmployeesView39
JOIN tblDepartmentView39
ON tblEmployeesView39.ID = tblDepartmentView39.DepartID


select * from VWNonConfidentionalData


Create View VWSumarizedDate
as
select DepartName, COUNT(ID) as TotalEmployees
from tblEmployeesView39
JOIN tblDepartmentView39
ON tblEmployeesView39.DepartmentID = tblDepartment.DepartmentID
Group by DepartName