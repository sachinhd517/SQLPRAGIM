

-- Along with Employee and their Manager name, we also want ot display their level the organizations.



Create Table tblEmployee51
(
  EmployeeId int Primary key,
  Name nvarchar(20),
  ManagerId int
)

Insert into tblEmployee51 values (1, 'Tom', 2)
Insert into tblEmployee51 values (2, 'Josh', null)
Insert into tblEmployee51 values (3, 'Mike', 2)
Insert into tblEmployee51 values (4, 'John', 3)
Insert into tblEmployee51 values (5, 'Pam', 1)
Insert into tblEmployee51 values (6, 'Mary', 3)
Insert into tblEmployee51 values (7, 'James', 1)
Insert into tblEmployee51 values (8, 'Sam', 5)
Insert into tblEmployee51 values (9, 'Simon', 1)

WITH EmployeesCTE (EmployeeId, Name, ManagerId, [Level])
as
(
	select EmployeeId, Name, ManagerId, 1
	from tblEmployee51
	where ManagerId IS NULL
	
	UNION ALL
		
	select tblEmployee51.EmployeeId, tblEmployee51.Name,
	tblEmployee51.ManagerId, EmployeesCTE.[Level]+1
	from tblEmployee51
	JOIN EmployeesCTE
	ON tblEmployee51.ManagerId = EmployeesCTE.EmployeeId
)
--select * from EmployeesCTE

select EmpCTE.Name as Employee, ISNULL(MgrCTE.Name, 'Super Boss')as Manager,
EmpCTE.[Level]
from EmployeesCTE EmpCTE
LEFT JOIN EmployeesCTE MgrCTE
ON EmpCTE.ManagerId = MgrCTE.EmployeeId
