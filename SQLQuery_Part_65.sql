-- -- T-SQL query to list all tables
-- Object explorer with in SQL server management studio can be used to get the list OF
-- table in a specific database. However, if we have to write a query to achieve the same, there are 3 system views that we can use.
-- 1. SYSOBJECTS - SQL Server 2000,2005 & 2008
-- 2. SYS.TABLES - SQL Server 2005 & 2008
-- 3. INFORMATION_SCHEMA.TABLES - SQL Server 2005 & 2008

-- https://docs.microsoft.com/en-us/sql/relational-databases/system-compatibility-views/sys-sysobjects-transact-sql?view=sql-server-2017

select * FROM sysobjects

select * from sysobjects WHERE xtype = 'U';

SELECT * from sysobjects WHERE xtype = 'FN';

SELECT * from sysobjects where xtype = 'p';

select * from sysobjects where xtype = 'v';

select distinct xtype FROM sysobjects

select * from sys.tables;

select * from sys.views;

select * from sys.procedures;

SELECT * FROM INFORMATION_SCHEMA.TABLES;

SELECT * FROM INFORMATION_SCHEMA.VIEWS;

SELECT * FROM INFORMATION_SCHEMA.ROUTINES;