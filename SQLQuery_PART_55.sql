--Error handling

--With the introduction of Try/Catch blocks in SQL Server 2005, error handling in sql 
--server, is no similar to programming languages like C#, and java.

--Error Handling in SQL Server 2000- @@Error
--Error Handling in SQL Server 2005 & later- Try....Catch

--Note- Sometimes, system functions that begin with two at singns(@@), are called as
--gobla variables. Thye are not variables and do not have the same behavious as 
--variabels, instead they are very similar to functions.

--RAISERROR('Error Message',ErrorSeverity,ErrorState)
--Create and retrun custom errors
--Severity level = 16(indicate general errors that can be corrected by the user)
--State = Numbers between 1 to 255. RAISERROR only generates errors with state 
--from 1 through 127.


--@@Error returns a NON-ZERO value, if there is an error, otherwise ZERO, indicating that
--the previous SQL statment encountered no errors.

--Note: @@ERROR si cleared and reset on each statement execution. Check it immediately
--following the statement being varified, or save it to a local variable that can be checked
--later.


Create Table tblProduct55
(
 ProductId int NOT NULL primary key,
 Name nvarchar(50),
 UnitPrice int,
 QtyAvailable int
)

Create Table tblProductSales55
(
 ProductSalesId int primary key,
 ProductId int,
 QuantitySold int
) 

select * from tblProduct55
select * from tblProductSales55

Insert into tblProduct55 values(1, 'Laptops', 2340, 100)
Insert into tblProduct55 values(2, 'Desktops', 3467, 50)

ALter table tblProduct55 add constraint FK_tblProduct55_ProductId
FOREIGN KEY (ProductId) references tblProduct55(ProductId)

EXEC spSellProduct 1, 10

ALTER Procedure spSellProduct 
@ProductId int,
@QuantityToSell int
as
Begin
	--Check the strock avaiable, for the rpducts we want to sell

	Declare @StockAvaiable int
	Select @StockAvaiable = QtyAvailable
	from tblProduct55 where ProductId = @ProductId

	--Throw an error to the calling application, of enough stock
	--is not available

	if(@StockAvaiable < @QuantityToSell)
		Begin
			Raiserror('Not enough stock avaiable', 16,1)
		End

	--If enough stock available

	Else
		Begin
			Begin Tran
				--First reduce the qiantity available
				Update tblProduct55 set QtyAvailable = (QtyAvailable - @QuantityToSell)
				where ProductId = @ProductId

				Declare @MaxProductSalesId int
				--Calculate MAX ProductDalesId
				select @MaxProductSalesId = CASE When	
					MAX(ProductSalesId) IS NULL
					THEN 0 ELSE MAX(ProductSalesId) end
					FROM tblProductSales55

				-- Increment @MaxProductSalesId by 1, so we don't get a primary key violation

				SET @MaxProductSalesId = @MaxProductSalesId + 1
				INSERT INTO tblProductSales55 VALUES(@MaxProductSalesId, @ProductId,@QuantityToSell)
				IF(@@ERROR<>0)
				BEGIN
					rollback TRAN
					print 'Transaction rolled back'
				END
				ELSE
				BEGIN
					COMMIT TRAN
					PRINT 'Transaction Committed'
				END
				
		END
END

--================================================================================================================

Insert into tblProduct55 values(2,'Mobile Phone', 1500, 100)
IF(@@ERROR<>0)
	print 'Error Occurred'
ELSE
	print 'No Error'

Insert into tblProduct55 values(2, 'Mobile Phone', 1500, 100)
--At this point @@ERROR will have a NON ZERO value 
Select * from tblProduct55
--At this point @@ERROR gets reset to ZERO, because the 
--select statement successfullyexecuted
if(@@ERROR <> 0)
 Print 'Error Occurred'
Else
 Print 'No Errors'


 Declare @Error int
Insert into tblProduct55 values(2, 'Mobile Phone', 1500, 100)
Set @Error = @@ERROR
Select * from tblProduct55
if(@Error <> 0)
 Print 'Error Occurred'
Else
 Print 'No Errors'
