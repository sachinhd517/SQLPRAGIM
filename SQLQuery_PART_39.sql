-- View in SQL SERVER PART - 39

--		What is a VIEW ?
--		A View is nothing more than a saved SQL query. A view can also be considered as a
--		Virtual table.



--Create Table tblEmployeesView
--(
--	[ID] int Primary key IDENTITY NOT NULL,
--	[Name] nvarchar(50) NOT NULL,
--	[Salary] nvarchar(50) NOT NULL,
--	[Gender] nvarchar(50) NOT NULL
--)



sp_help tblDepartmentView
sp_help tblEmployeeView

CREATE TABLE tblEmployees39
(
	[Id] int identity NOT NULL,
	[Name] nvarchar(50),
	[Salary] int,
	[Gender] nvarchar(50),
	[DepartmentId] int
)

Create Table tblDepartment39
(
	[DepartID] int PRIMARY KEY IDENTITY NOT NULL,
	[DepartName] nvarchar(50) NOT NULL 
)

insert into tblEmployees39 values('John',5000,'Male',3)
insert into tblEmployees39 values('Mike',3400,'Male',2)
insert into tblEmployees39 values('Pam',6000,'Female',1)
insert into tblEmployees39 values('Todd',4800,'Male',4)
insert into tblEmployees39 values('Sara',3200,'Female',1)
insert into tblEmployees39 values('Ben',4800,'Male',3)


insert into tblDepartment39 values('IT')
insert into tblDepartment39 values('Payroll')
insert into tblDepartment39 values('HR')
insert into tblDepartment39 values('Admin')


select * from tblEmployees39
select * from tblDepartment39



alter table tblEmployees39 add constraint FK_tblEmployees39_DepartmentId
FOREIGN KEY (DepartmentId) references tblDepartment39(DepartID)

sp_depends tblEmployees39

select  ID,Name,Salary,Gender,DepartName
from tblEmployees39
JOIN tblDepartment39
ON tblEmployees39.ID = tblDepartment39.DepartID

Create View VWEMployeeByDepartment
as
select ID,Name,Salary,Gender,DepartName
from tblEmployees39
JOIN tblDepartment39
ON tblEmployees39.ID = tblDepartment39.DepartID

select * from VWEMployeeByDepartment

sp_helptext VWEMployeeByDepartment

select * from tblEMployeeView
select * from tblDepartmentView

select * from VWEMployeeByDepartment

sp_helptext VWEMployeeByDepartment

--Advantages of Views

--Views can be used to reduce the complexity of the database
--sechema

--View can be used as a mechannisum to implement rwo and 
--column level security.

--View can be used to present aggregated data and hide
--detailed data.

--To modify a view - ALTER VIEW statment
--TO DROP a view - DROP VIEWvWName

select ID,Name,Gender,DepartName
from tblEmployees39
JOIN tblDepartment39
ON tblEmployees39.ID = tblDepartment39.DepartID
where tblDepartment39.DepartName = 'IT'

Create View VWITEmployees
as
select ID,Name,Gender,DepartName
from tblEmployees39
JOIN tblDepartment39
ON tblEmployees39.ID = tblDepartment39.DepartID
where tblDepartment39.DepartName = 'IT'

select * from VWITEmployees

Create View VWNonConfidentionalData
as
select ID,Name,DepartName
from tblEmployees39
JOIN tblDepartment39
ON tblEmployees39.ID = tblDepartment39.DepartID


select * from VWNonConfidentionalData


Create View VWSumarizedDate
as
select DepartName, COUNT(ID) as TotalEmployees
from tblEmployees39
JOIN tblDepartment39
ON tblEmployees39.DepartmentID = tblDepartment39.DepartID
Group by DepartName


select * from VWSumarizedDate