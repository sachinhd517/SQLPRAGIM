--Transaction ACID Test

--A transaction is a group of database commands that are treated as single unit. A
--successfuly transaction must pass the "ACID" test, that is, it must be

--Atomic -  All statements in the transaction either completed successfully or they
--were all rolled back. The task taht the set of operations represents is either
--accomplished or not, but in any case not left half-done


--Consistent - All data toruched by the transaction is left in a logically consistent state.
--For example, if stock available mumbers are decremented from tblPorductTable,
--then, there has to be a realted entry in tblProductSales table. The inventory can't 
--just disaplear.


--Isolated: The transaction must affect data without interforming with other concurrent
--transactions, or being interfered with by them. This prevents transaction from
--making changes to data based on uncommitted information, for example changes 
--to a record that are subsequently rolled back. Most databasees use locking to
--maintain transaction isolation.

--Durable: Once a change is made, it is permanent. If a system  error or power failure
--occurs before a set of commands is complete, those commands are undone and the
--data is resotred to its original state once the system begin running  again.

--Note session : Isolation  Levels in SQL Server

  

Create Table tblProduct58
(
 ProductId int NOT NULL primary key,
 Name nvarchar(50),
 UnitPrice int,
 QtyAvailable int
)

Create Table tblProductSales58
(
 ProductSalesId int primary key,
 ProductId int,
 QuantitySold int
) 

select * from tblProduct58
select * from tblProductSales58

Insert into tblProduct58 values(1, 'Laptops', 2340, 90)
Insert into tblProduct58 values(2, 'Desktops', 3467, 50)

