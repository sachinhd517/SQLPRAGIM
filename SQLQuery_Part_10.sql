--Use sql server vidio tutorial Part 10
Create table Table_Person10
(
	[ID] int primary key NOT NULL,
	[Name] nvarchar(50) NOT NULL,
	[Email] nvarchar(50) NOT NULL,
	[GenderId] int NOT NULL,
	[Age] int NOT NULL,
	[City] nvarchar(50)
)

insert into Table_Person10 values(1,'Tom','t@tgamil.com',1,20,'London')
insert into Table_Person10 values(2,'John','j@agamil.com',1,20,'New York')
insert into Table_Person10 values(3,'Mary','m@mgamil.com',1,21,'Sydney')
insert into Table_Person10 values(4,'John','John@tgamil.com',1,29,'London')
insert into Table_Person10 values(5,'Sara','sara@tgamil.com',1,25,'Mumbai')
insert into Table_Person10 values(6,'MaryKim','Maritgamil.com',2,22,'Goa')

select * from Table_Person10

select DISTINCT city,Email from Table_Person10

select city,Email from Table_Person10

select * from Table_Person10 where city = 'London'

select * from Table_Person10 where City <> 'London'

select * from Table_Person10 where City != 'London'

select * from Table_Person10 where Age = 20 OR Age = 23 OR Age = 25

select * from Table_Person10 where Age IN (23,32,25)

select * from Table_Person10 where Age BETWEEN 20 AND 24

select * from Table_Person10 where City LIKE 'L%'

select * from Table_Person10 where Email LIKE '%@%'

select * from Table_Person10 where Email NOT LIKE '%@%'

select * from Table_Person10 where Email LIKE '_@_.com'

select * from Table_Person10 where Name LIKE '[MST]%'

select * from Table_Person10 where Name LIKE '[^MST]%'

select * from Table_Person10 where (City = 'London' OR City = 'Mumbai') AND Age > 23

select * from Table_Person10 order by ID desc

select * from Table_Person10 order by Name desc, Age asc

select TOP 50 * from Table_Person10

select TOP 2 * from Table_Person10 order by Age desc

select TOP 1 Percentage * from Table_Person10

select TOP 2 Name, Age from Table_Person10

select * from Table_Person10

select TOP 1 * from Table_Person10 order by GenderID DESC


