USE[SQLSERVERPRAGIM]
GO

CREATE TABLE tblEmployee67
(
    ID int PRIMARY KEY IDENTITY NOT NULL,
    Name NVARCHAR(100),
    Gender NVARCHAR(100),
    Salary NVARCHAR(100)
)


Insert into tblEmployee67 values('Sara Nani','Female','4500')
Insert into tblEmployee67 values('James Histo','Male','5300')
Insert into tblEmployee67 values('Mary Jane','Female','6200')
Insert into tblEmployee67 values('Paul Sensit','Male','4200')
Insert into tblEmployee67 values('Mike Jen','Male','5500')

SELECT * FROM tblEmployee67

select SUM(Salary) as TOTALSALARY FROM tblEmployee67
GROUP BY Gender


ALTER TABLE tblEmployee67 ALTER COLUMN Salary INT