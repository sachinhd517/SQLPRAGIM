-- COALESCE FUNCTION  INSQL SERVER PART 16

CREATE TABLE EmployeePart16
(
	[ID] int IDENTITY NOT NULL,
	[FirstName] nvarchar(50),
	[MiddleName] nvarchar(50),
	[LastName] nvarchar(50)

)

select * from EmployeePart16

insert into EmployeePart16 values('Sam','NULL','NULL')
insert into EmployeePart16 values('NULL','Todd','Tarzen')
insert into EmployeePart16 values('NULL','NULL','Sara')
insert into EmployeePart16 values('Sen','Parker','NULL')
insert into EmployeePart16 values('Jemes','Nick','Nancy')


CREATE TABLE tbl_EmployeePart14
(
	[EmployeeID] int IDENTITY NOT NULL,
	[Name] nvarchar(50) NOT NULL,
	ManagerID nvarchar(50)
)

select * from tbl_EmployeePart14

insert into tbl_EmployeePart14 values('Mike',3)
insert into tbl_EmployeePart14 values('ROb',1)
insert into tbl_EmployeePart14 values('Todd',NULL)
insert into tbl_EmployeePart14 values('Ben',1)
insert into tbl_EmployeePart14 values('Sam',1)

select *
from	EmployeePart16 as A
LEFT JOIN tbl_EmployeePart14 as B
ON	A.ID = B.EmployeeID

use [SQL-SERVER-PRAGIM-DATABASE]
GO

select * from EmployeePart16

select ID, COALESCE(FirstName,MiddleName,LastName) as Name
from EmployeePart16

select ID, COALESCE(FirstName, MiddleName, LastName) as Name from EmployeePart16

