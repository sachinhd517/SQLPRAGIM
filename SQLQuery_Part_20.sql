--Output Parameters or Returns Values
_____________________________________________________________________________________
	--Return Status Value			|			Output Parameters					 |	
____________________________________|________________________________________________|
--Only INteger Datatype				|	Any Datatype								 |
--Only one value					|	More than value								 |
--Use to convey success or failure	|	Use to return values like name, count etc.,	 |
									|												 |
____________________________________|________________________________________________|


create table tblEmployee20
(
	[Id] int primary key NOT NULL,
	[Name] nvarchar(50) NOT NULL,
	[Gender] nvarchar(50) NOT NULL,
	[DepartmentId] int NOT NULL
)
Create table tblDepartment20
(
	[ID] int primary key,
	[Dept_Name] nvarchar(50)
)


insert into tblEmployee20 values(1,'sam','Male',1)
insert into tblEmployee20 values(2,'Ram','Male',1)
insert into tblEmployee20 values(3,'Sara','Female',3)
insert into tblEmployee20 values(4,'Todd','Male',2)
insert into tblEmployee20 values(5,'John','Male',3)
insert into tblEmployee20 values(6,'Sana','Female',2)
insert into tblEmployee20 values(7,'James','Male',1)
insert into tblEmployee20 values(8,'Rob','Male',2)
insert into tblEmployee20 values(9,'Steve','Male',1)
insert into tblEmployee20 values(10,'Pam','Female',2)


insert into tblDepartment20 values(1,'Devloper')
insert into tblDepartment20 values(2,'HR')
insert into tblDepartment20 values(3,'Account')

sp_depends tblEmployee20



--Alter table tblEmployee19 add constraint FK_tblEmployee19_GenderId
--Foreign key (DepartmentId) references tblDepartment19(ID)

Alter table tblEmployee20 add constraint FK_tblEmployee20_GenderId
Foreign key (DepartmentId) references tblDepartment20(ID)

Create proc spGetNameById
@Id int,
@Name nvarchar(50) output
as
Begin
	select @Name = Name from tblEmployee20 where id = @Id
End


Create proc spGetTotalCountOfEmployees20 
@TotalCount int out
as
Begin
	select @TotalCount = COUNT(Id) from tblEmployee20
END

Declare @Total int
EXEC spGetTOtalCountOfEmployees20 @Total out
Print @Total

Create Proc spGetTotalCount200
as
Begin
	return (select COUNT(Id) from tblEmployee20)
END

Declare @Total int
exec @Total = spGetTotalCount200
print @Total


Create proc spGetNameById201
@Id int,
@Name nvarchar(50) output
as
Begin
	select @Name = Name from tblEmployee20 where Id = @Id
End

Declare @Name nvarchar(40)
exec spGetNameById201 1, @Name OUT
Print 'Name = ' + @Name

Create proc spGetNameById2
@Id int
as
Begin
	return (select Name from tblEmployee20 where Id = @Id)
END

Declare @Name nvarchar(50)
EXEC @Name = spGetNameById2 1
print 'Name = ' + @Name