use [WCF_DATABASE]
GO

create table tblEmployee
(
	[id] int,
	[Name] nvarchar(50),
	[Gender] nvarchar(50),
	[DateOfBirth] datetime
)

delete from tblEmployee

Alter table tblEmployee Add 
EmployeeType int, AnnualSalary int, HourlyPay int, HoursWorked int

select * from tblEmployee

select Name,Gender from  tblEmployee where id=4

insert into tblEmployee values(1,'Mark','Male','10/10/1990')
insert into tblEmployee values(2,'Mary','Female','11/10/1981')
insert into tblEmployee values(3,'John','Male','8/10/1979')
insert into tblEmployee values(4,'Rajesh','Male','10/12/1990')
insert into tblEmployee values(5,'MaryCorn','Female','11/10/1981')
insert into tblEmployee values(3,'John Abrahim','Male','8/10/1979')


Create procedure spGetEmployee
@Id int
as 
Begin
	select Id, Name, Gender, DateOfBirth from tblEmployee where Id = @Id
End

Alter procedure spGetEmployee
@Id int
as 
Begin
	select Id, Name, Gender, DateOfBirth, EmployeeType, AnnualSalary, HoursWorked from tblEmployee where Id = @Id
End

Create procedure spSaveEmployee
@Id int,
@Name nvarchar(50),
@Gender nvarchar(50),
@DateOfBirth DateTime
as
Begin
	insert into tblEmployee values(@Id, @Name, @Gender, @DateOfBirth)
End


Alter procedure spSaveEmployee
@Id int,
@Name nvarchar(50),
@Gender nvarchar(50),
@DateOfBirth DateTime,
@EmployeeType int,
@AnnualSalary int = null,
@HourlyPay int = null,
@HoursWorked int = null
as
Begin
	insert into tblEmployee values(@Id, @Name, @Gender, @DateOfBirth, @EmployeeType,@AnnualSalary, @HourlyPay, @HoursWorked )
End
