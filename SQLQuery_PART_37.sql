--Unique and Non Unique Index in SQL server PART - 37

-- Unique Index

--Unique index is used to enforce uniqueness key values in the index.

Create Table tblEmployee37
(
	[ID] int primary key,
	[FirstName] nvarchar(50),
	[LastName] nvarchar(50),
	[Salary] int,
	[Gender] nvarchar(50),
	[City] nvarchar(50)
)

--Note : By Default, PRIMARY KEY constraint, create a unique clustered index.

--UNIQUESNESS is a property of an index, and both CLUSTERED and NON-CLUSTERED
--index can be UNIQUE.

--Difference between Unique Constraint and Unique index

	--There are no major differences between a unique constraint and a unique index. In fact,
	--when you add a unique constraint, a unique index gets created behind the scenes.

sp_helpindex tblEmployee37

drop index tblEmployee37.PK__tblEmpl__3214EC27531F3BE2


insert into tblEmployee37 values(1,'Mike','sandoz',4500,'Male','New York')
insert into tblEmployee37 values(1,'San','Meneo',2500,'Male','London')

select * from tblEmployee37

drop table tblEmployee37

Alter table tblEmployee37
add constraint UQ_tblEmployee_city
UNIQUE (City)

Alter table tblEmployee37
add constraint UQ_tblEmployee_city
UNIQUE CLUSTERED(City)
