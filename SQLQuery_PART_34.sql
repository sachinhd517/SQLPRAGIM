--Temporary Table in SQL SERVER PART- 34

--Temporary Tables
	
--	Temporary tables, are very similar to the permanent tables. Permanent tables get
--	created in the database you specify, and remain in the database permanently, yntil you
--	delete (drop) them. On the other hand, temporary tables get created in the TempDB and
--	are automatically deleted, when they are no longer used.

--Different Types of Temporary tables
	
--	1.	Local Temporary Table
--	2.	Global TEmporary tables.
		
--Example :

----1. Grphical view 
--  2. You should show temporary table in system database in Tempdb go to the Temporary Table  

Create Table #PersonDetails34(ID int, Name nvarchar(20))

insert into #PersonDetails34 values(1,'Mike')
insert into #PersonDetails34 values(2,'John')
insert into #PersonDetails34 values(3,'Todd')

select * from #PersonDetails34

--2.	Query view

--LOCAL TEMPORARY TABLES

--Check if the local temporary table is created:
--Temporary tables are created in the TEMPDB.
--Query the sysobjects system table in
--TEMPDB. The name of the table, is suffixed
--with lot of underscores and randowm number.
--For this reson you have to use the LIKE operator in the query.

--local temporary table is available, only for the connection
--that has created the table

--A local temporary table is automatically dropped, when the connection that
--has created the it, is closed.

--If the user want ot explicity drop the temporary table, he can do so usig DROP
--TABLE #PersonDetails

--It's donoted as # symbole 

select Name from tempdb..sysobjects
where Name like '#PersonDetails%'

--------------------------------------


CREATE PROCEDURE spCreateLocalTempTable34
as
Begin

Create Table #PersonDetails34(ID int, Name nvarchar(20))

insert into #PersonDetails34 values(1,'Mike')
insert into #PersonDetails34 values(2,'John')
insert into #PersonDetails34 values(3,'Todd')

select * from #PersonDetails34
END

EXEC spCreateLocalTempTable34

select * from #PersonDetails34

--------------------------------------------------------------------------------
--Global Temporary Table

--To create a Golbal Temporary Table, Prefix the name of the table with 2 pound(##) symbols.

--create Table ##EmployeeDetails(ID int, Name nvarchar(20))

--Global temporary tables are visible to all the connection of the sql server, and are only
--destroyed when the last connection referencing the table is closed.

--Multiple users, across multipe connections can have local temporary tables with the
--same name, but, a global temporary table name has to be unique, and if you insepect the
--name o fht global temp table, in the object explore, there will be no randowm numbers
--suffixed at the end of the table name.

Create table ##EmployeeDetails(ID int, Name nvarchar(20))
--------------------------------------------------------------------------------

--Difference Between Temporary Table and Global Temp table

--1.	Loca Temp tables are prefixed with single pound(#) symbol, where as gloabl temp
--		table are prefixed with 3 pound(##) symbols.

--2.	SQL Server appends some random numbers at the end of the local tenp table name,
--		where this is not done for global temp table names.

--3.	Localtemporary table are only visible to the session of the SQL Server which has
--		created it, where as Globel temporary table are visible to all SQL server sessions

--4.	Local temporary tables are automatically dropped, when the session that created the
--		temporary tables is closed, where as Global temporary tables are destroyed when the
--		last connection that is referncing the global temp table is closed.




