CREATE TABLE tblEmployee44
(
  [Id] int Primary Key,
  [Name] nvarchar(30),
  [Salary] int,
  [Gender] nvarchar(10),
  [DepartmentId] int
)


Insert into tblEmployee44 values (1,'John', 5000, 'Male', 3)
Insert into tblEmployee44 values (2,'Mike', 3400, 'Male', 2)
Insert into tblEmployee44 values (3,'Pam', 6000, 'Female', 1)
Insert into tblEmployee44 values (4,'Todd', 4800, 'Male', 4)  
Insert into tblEmployee44 values (5,'Sara', 3200, 'Female', 1) 
Insert into tblEmployee44 values (6,'Ben', 4800, 'Female', 3) 

CREATE TABLE tblEmployeeAudit44
(
  Id int identity(1,1) primary key,
  AuditData nvarchar(1000)
)

Insert into tblEmployeeAudit44 values ('IT')
Insert into tblEmployeeAudit44 values ('Payroll')
Insert into tblEmployeeAudit44 values ('HR')
Insert into tblEmployeeAudit44 values ('Admin')

select * from tblEmployee44 
select * from tblEmployeeAudit44

ALTER TABLE tblEmployee44 add constraint FK_tblEmployee44_DepartmentId
FOREIGN KEY (DepartmentId) references tblEmployeeAudit44(Id)

use [SQL-SERVER-PRAGIM-DATABASE]
GO
CREATE TRIGGER Tr_tblEmployee44_ForInsert
ON tblEmployee44
for INSERT
AS 
BEGIN
		Declare @Id int
		select @Id = Id from inserted
		insert into tblEmployee44 values('New Employee with ID = ' + CAST(@Id as nvarchar(50)) + 'is added at ' + CAST(GETDATE() as nvarchar(100)))
END



Create Trigger tr_tblEmployee44_forUpdate
on tblEmployee44
for Update
as
Begin
	select * from deleted
	select * from inserted
End

update tblEmployee44 set Name = 'Valarieyy', Salary = 5500, Gender = 'Female' where ID = 8 
-----------------------------------------------------------

Create trigger tr_tblEmployee_ForUpdate
ON tbl_Employees
for Update
as
Begin
		Declare @Id int
		Declare @oldName nvarchar(20), @NewName nvarchar(20)
		Declare @oldSalary int, @NewSalary int
		Declare @oldGender nvarchar(20), @NewGender nvarchar(20)
		Declare @oldDeptId int, @NewDeptId int

		Declare @AuditString nvarchar(1000)

		select * into #TempTable
		from inserted

		while (Exists(Select Id from #TempTable))
		Begin

			set @AuditString = ''

			select Top 1 @Id = Id, @NewName = Name,
			@NewGender = Gender, @NewSalary = Salary,
			@NewDeptId = @oldDeptId
			from #TempTable

			select @oldName = Name, @oldGender = Gender,
			@oldSalary = Salary, @oldDeptId = DepartmentID 
			from deleted where Id = @Id
			
			set @AuditString = 'Employee with Id = ' + Cast(@Id as nvarchar(4)) + ' Changed ' 
			if(@oldName <> @NewName)
					set @AuditString = @AuditString + 'NAME from ' + @oldName + ' to ' + @NewName

			if(@oldGender <> @NewGender)
					set @AuditString = @AuditString + 'GENDER from ' + @oldGender + ' to ' + @NewGender 

			if(@oldSalary <> @NewSalary)
					set @AuditString = @AuditString + 'SALARY from' + cast( @oldSalary as nvarchar(10)) + ' to ' + Cast(@NewSalary as nvarchar(400))

			if(@oldDeptId <> @NewDeptId)
					set @AuditString = @AuditString + 'DepartmentId From ' + CAST(@oldDeptId as nvarchar(10)) + ' to ' + cast(@NewDeptId as nvarchar(50))

			insert into tbl_Employees values (@AuditString)

			Delete from #TempTable where Id = @Id
			
		End
End
