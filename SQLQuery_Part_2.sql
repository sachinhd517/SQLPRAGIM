--Creating and Altering a Database

--A SQL Server database can be created, altered and dropped

--1.	Graphically using SQL Server Management Studio (SSMS) or
--2.	Using a Query

--TO Create the database usig a query
--Create database DatabaseName

--Whether, you create a database graphically using the designer or, using a query, the following  2 files gets generated

--.MDF file - Data File (Contains actual data)
--.LDF file - Transaction Log file (Used to recover the database)

--To alter a database, once it's created
--Alter databse DatabaseName Modify Name =  NewDatabaseName

--Alternatively, you can also use system stroed procedure
--Execute sp_renameDB 'OldDatabaseName','NewDatabaseName'

create database sample1

alter database sample1 modify name = sample3

--using stored Procedure

sp_renameDB 'sample3','sample4'

--To Delete or Drop a databse
--Drop Database DatabaseThatYouWantToDrop

--Dropping a database, deletes the LDF and MDF file

--You cannot drop a database, if it is currently in use. YOu get an error starting - cannot drop database
--"NewDatabaseName" beacause it is currently is use

--So, if Other users are connected, you need to put the database in single user mode and then drop the database.

--Alter Database DatabaseName set SINGLE_USER With Rollback Immediate

--With Rollback Immediate option, will rollback all incomplete transaction and closed the connection to the databse.

--Note : System database cannot be dropped.


Drop DATABASE sample4