--Clustrered On Non Clustered in SQL server PART - 36
--Index Types :

--1.	Clustered
--2.	Nonclustered
--3.	Unique
--4.	Filtered
--5.	XML
--6.	Full Text
--7.	Spatical
--8.	Columnstore
--9.	Index with inculded columns
--10.	Index on computed columns

--In this session : Clustered and Nonclustered

--DIFFERENCE BETWEEN Cluster index and Non-cluster index

--1.	Only one clustered index per table, where as you can have then on
--		non clustered index

--2.	Clustered index is faster then a non clustered index, because, the clustered 
--		index has to refer back to the table, if the selected column is not present in the
--		index

--3.	Clustered index determines the stroage order of row in the table. and hence
--		doesn't require additional disk space, but where as a non clustered index is
--		stored separately from the table, additional stroage space is required.


Create table tblEmployee36
(
	[ID] int Primary Key,
	[Name] nvarchar(50),
	[Salary] int,
	[Gender] nvarchar(20),
	[City] nvarchar(50)
)

sp_helpindex tblEmployee36

insert into tblEmployee36 values(3,'john',4100,'Male','New York')

insert into tblEmployee36 values(1,'Sam',2500,'Male','London')


insert into tblEmployee36 values(4,'Sara',5500,'Female','Tokyo')

insert into tblEmployee36 values(5,'Todd',3100,'Male','Toranto')


insert into tblEmployee36 values(2,'Pam',6500,'Female','Sydney')

select * from tblEmployee36

drop  table tblEmployee36

Create Clustered Index IX_tblEmployee_Gender_Salary
ON tblEmployee36 (Gender DESC, Salary ASC)

drop Index tblEmployee.PK__tblEmplo__3214EC279E7D4678


Create NonClustered Index IX_tblEmployee_Name
ON tblEmployee(Name)