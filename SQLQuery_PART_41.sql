----INDEXED VIEW IN SQL SERVER IN PART - 41

--What is an Indexed View?
	
--		OR
--What happens when you create an index on a view?

--A standard or Non-indexed view, is just a stored SQL query. When, we try to
--retrieve data from the view, the data is actually retrieved from the underlying
--base tables.

--So, a view is just a virtual table it does not store any data, by default.

--However, when you create an index, on a view, the view gets materialized. This
--means, the view is no, capable of storing data.

--In SQL server, we call them indexed views and in Oracle, Materialized views.

--Guidleine for creating indexed Views

--1.	The view should be created with SchemBinding option
--2.	If an Aggregate function in the SELECT LIST, reerences an expression, and if there is a 
--		possibility for that expression to become NULL, then, a replacement values should be
--		specified.
--3.	If GROUP BY is specified, the view select list must contain a COUNT_BIG(*) expression
--4.	The base tables in the view, should be referenced with 2 part names.

CREATE TABLE tblProducts41
(
	[ProductId] int primary key NOT NULL,
	[Name] nvarchar(50),
	[UnitPrice] int
)
CREATE TABLE tblProductsSales41
(
	[ProductId] int  NOT NULL,
	[QuentitySold] int
)


insert into tblProducts41 values(1,'Books',20)
insert into tblProducts41 values(2,'Pens',14)
insert into tblProducts41 values(3,'Pencils',11)
insert into tblProducts41 values(4,'Clips',10)

insert into tblProductsSales41 values(1,10)
insert into tblProductsSales41 values(3,23)
insert into tblProductsSales41 values(4,21)
insert into tblProductsSales41 values(2,12)
insert into tblProductsSales41 values(1,13)
insert into tblProductsSales41 values(3,12)
insert into tblProductsSales41 values(4,13)
insert into tblProductsSales41 values(1,11)
insert into tblProductsSales41 values(2,12)
insert into tblProductsSales41 values(1,14)

select * from tblProducts41
select * from tblProductsSales41

alter table tblProducts41 add constraint FK_tblProducts41_Products41
FOREIGN KEY (ProductId) references tblProductsSales41(ProductId)

CREATE VIEW VWTotalSalaesByProduct
WITH SchemaBinding
as
Select Name, SUM(ISNULL((QuentitySold * UnitPrice), 0)) as TotalSales,
COUNT_BIG(*) as TotalTransactions
from [dbo].[tblProductsSales41]
join	[dbo].[tblProducts41]
ON		[dbo].[tblProducts41].ProductId = [dbo].[tblProductsSales41].ProductId
GROUP BY Name

select * from VWTotalSalaesByProduct

create UNIQUE Clustered Index UIX_vWTotalSalesByProducts_Name 
on VWTotalSalaesByProduct(Name)