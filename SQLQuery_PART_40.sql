--UPDATABLE VIEW IN SQL SERVER - PART 40
use [ASP_DATABASE]
GO

CREATE TABLE tblEmployees40
(
	[Id] int identity NOT NULL,
	[Name] nvarchar(50),
	[Salary] int,
	[Gender] nvarchar(50),
	[DepartmentId] int
)

Create Table tblDepartment40
(
	[DepartID] int PRIMARY KEY IDENTITY NOT NULL,
	[DepartName] nvarchar(50) NOT NULL 
)

insert into tblEmployees40 values('John',5000,'Male',3)
insert into tblEmployees40 values('Mike',3400,'Male',2)
insert into tblEmployees40 values('Pam',6000,'Female',1)
insert into tblEmployees40 values('Todd',4800,'Male',4)
insert into tblEmployees40 values('Sara',3200,'Female',1)
insert into tblEmployees40 values('Ben',4800,'Male',3)


insert into tblDepartment40 values('IT')
insert into tblDepartment40 values('Payroll')
insert into tblDepartment40 values('HR')
insert into tblDepartment40 values('Admin')


select * from tblEmployees40
select * from tblDepartment40



alter table tblEmployees40 add constraint FK_tblEmployees40_DepartmentId
FOREIGN KEY (DepartmentId) references tblDepartment40(DepartID)


select * from tblEmployees40

Create View VWEmployeeDataExceptSalary
as
select ID, Name, Gender, DepartmentID
from tblEmployees40

select * from VWEmployeeDataExceptSalary

Update VWEmployeeDataExceptSalary
set Name = 'Mikey' where ID = 2

Delete from VWEmployeeDataExceptSalary where ID = 2

Insert into VWEmployeeDataExceptSalary values('Mike',2540,'Male')

Create View VWEmployeeDetailsByDepartment
as
select ID, Name, Salary, Gender, DepartName
from tblEmployees40
JOIN tblDepartment40
ON tblEmployees40.DepartmentID = tblDepartment40.DepartID

select * from VWEmployeeDetailsByDepartment

Update VWEmployeeDetailsByDepartment
set DepartName = 'IT' where Name = 'John'


select * from tblEmployees40
select * from tblDepartment40

--Conclusion - If view is based on multiple tables, and if you update the view, it may not
--update the underlying base tables correctly. To correctly update a view, that is bases on
--multiple table, INSTEAD triggers are used.

--We will discuss about triggers and correctly updating a view that is based on multiple
--tables, in a later video seesion.

