-- LEFT, RIGHT, CHARINDEX AND SUBSTRING FUNCTION IN SQL SERVER PART - 23

--1. LEFT(Character_Expression, Integer_Expression)	: Return the specified number of chareacters form the left hand side of the gievn character expression.
--2. RIGHT(Character_Expression, Integer_Expression)	: Return the specified number of characters from the right ahd side of the given character expression.
--3. CHARINDEX('Expression_To_Find',
--   'Expression_To_Search','Start_Location')         : Returns the starting position of the specified expression in a characters string.
--4. SUBSTRING('Expression','Start','Length')			: Returns substring(Part of the string), from the given expression


select LEFT('ABCDEFGH',3)

select RIGHT('ABCDEFGH',4)

select CHARINDEX('@','sachinhd517@gmail.com',1)

select CHARINDEX('@','sachinhd517@gmail.com')

select SUBSTRING('sachinhd517@gmail.com',13,5)

