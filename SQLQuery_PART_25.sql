--DATETIME FUNCTION IN SQL SERVER - PART 25

--UTC Stands for Coordinated Universal Time, based on which, the world
--regulates clocks and time. There are slight differences between GMT
--and UTC, but for most common purpose, UTC is synonymous with GMT.

--GETDATE()				:	Commonly used
--CURRENT_TIMESTAMP		:	ANSI SQL equovalent to GETDATE
--SYSDATETIME()			:	More fractional seconds precision
--SYSDATETIMEOFFSET()		:	More fractional seconds precision + Time zone offset
--GETUTCDATE()			:	UTC Date and Time
--SYSUTCDATETIME()		:	UTC Date and Time, with more fractional seconds precision

use [ASP_DATABASE]
GO

select * from tblDateTime 

insert into tblDateTime values(GETDATE(),GETDATE(),GETDATE(),GETDATE(),GETDATE(),GETDATE())

update tblDateTime SET c_datetimeoffset = '2017-02-11 12:14:29.5700000 +10:00'
where c_datetimeoffset = '2017-02-11 12:14:29.5700000 +00:00'

select GETDATE()

select CURRENT_TIMESTAMP

select SYSDATETIME(

select SYSDATETIMEOFFSET()

select GETUTCDATE()

select SYSUTCDATETIME()
