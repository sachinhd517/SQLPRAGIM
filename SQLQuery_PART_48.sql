CREATE TABLE tblEmployee48
(
  Id int Primary Key,
  Name nvarchar(30),
  Salary int,
  Gender nvarchar(10),
  DepartmentId int
)


Insert into tblEmployee48 values (1,'John', 5000, 'Male', 3)
Insert into tblEmployee48 values (2,'Mike', 3400, 'Male', 2)
Insert into tblEmployee48 values (3,'Pam', 6000, 'Female', 1)
Insert into tblEmployee48 values (4,'Todd', 4800, 'Male', 4)  
Insert into tblEmployee48 values (5,'Sara', 3200, 'Female', 1) 
Insert into tblEmployee48 values (6,'Ben', 4800, 'Female', 3) 

CREATE TABLE tblDepartment48
(
  DeptId int identity(1,1) primary key,
  DepartmentName nvarchar(1000)
)

Insert into tblDepartment48 values ('IT')
Insert into tblDepartment48 values ('Payroll')
Insert into tblDepartment48 values ('HR')
Insert into tblDepartment48 values ('Admin')

select * from tblEmployee48 
select * from tblDepartment48

ALTER TABLE tblEmployee48 add constraint FK_tblEmployee48_DepartmentId
FOREIGN KEY (DepartmentId) references tblDepartment48(DeptId)


CREATE View VW_EmployeeCount48
as
select  DepartmentName, DepartmentId, COUNT(*) as TotalEmployees
from tblEmployee48
JOIN tblDepartment48
ON tblEmployee48.Id = tblDepartment48.DeptId
group by DepartmentName, DepartmentId

select * from VW_EmployeeCount48

select DepartmentName, TotalEmployees from VW_EmployeeCount48
where TotalEMployees >= 2

select DepartmentName, DepartmentId, COUNT(*) as TotalEmployees
into #TemptblEmployeeCount48
from tblEmployee48
JOIN tblDepartment48
ON	 tblEmployee48.DepartmentId = tblDepartment48.DeptId
GROUP BY DepartmentName, DepartmentId

select * from #TemptblEmployeeCount48

select DepartmentName, TotalEmployees
from #TemptblEmployeeCount48
where TotalEmployees >= 2

Declare		@tblEmployeeCount48 table(DepartmentName nvarchar(50),DepartmentId int, TotalEmplyees int)
select		DepartmentName, DepartmentId, COUNT(*) as TotalEmployees
from		tblEmployee48
JOIN		tblDepartment48
ON			tblEmployee48.DepartmentId = tblDepartment48.DeptId
GROUP BY	DepartmentName, DepartmentId

SELECT	DepartmentName, TotalEmplyees
FROM	@tblEmployeeCount48
WHERE	TotalEmplyees >= 2

--Using Derived Table

select DepartmentName, TotalEmployees
from
	(
		select DepartmentName, DepartmentId, COUNT(*) as TotalEmployees
		from tblEmployee48
		JOIN tblDepartment48
		ON	 tblEmployee48.DepartmentId = tblDepartment48.DeptId
		GROUP BY DepartmentName, DepartmentId
	)
as EmployeeCount
where TotalEmployees >= 2

--Using CTE

With EmployeeCount48(DepartmentName,DepartmentId, TotalEmployees) 
as
(
		select DepartmentName, DepartmentId, COUNT(*) as TotalEmployees
		from tblEmployee48
		JOIN tblDepartment48
		ON	 tblEmployee48.DepartmentId = tblDepartment48.DeptId
		GROUP BY DepartmentName, DepartmentId
)
select	DepartmentName, TotalEmployees
FROM	EmployeeCount48
WHERE	TotalEmployees >= 2	


--Note: A CTC can be thought of as a temporary result set that is defined within the
--execution scope of a single SELECT, INDERT, UPDATE, DELETE, or CREATE VIEW 
--statement. A CTE is similar to a derived table in that it is not stored as an object and 
--lasts only for the duration of the query.


