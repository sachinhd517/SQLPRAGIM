--ADVANCED OR INTRLLIGENT JOIN IN SQL SERVER - PART-13

use [ASP_DATABASE]
GO

select * from tbl_Employees;
select * from tbl_Department;

select Name, Gender, Salary, DepartmentName
from tbl_Employees 
LEFT JOIN tbl_Department
ON tbl_Employees.ID = tbl_Department.ID
where tbl_Employees.DepartmentID IS NULL

select Name, Gender, Salary, DepartmentName
from tbl_Employees
LEFT JOIN tbl_Department
ON tbl_Employees.DepartmentID = tbl_Department.ID
where DepartmentID IS NULL

select Name, Gender, Salary, DepartmentName
FROM tbl_Employees
RIGHT JOIN tbl_Department
ON tbl_Employees.DepartmentID = tbl_Department.ID

select Name, Gender, Salary, DepartmentName
FROM tbl_Employees
RIGHT JOIN tbl_Department
ON tbl_Employees.DepartmentID = tbl_Department.ID
WHERE tbl_Employees.DepartmentID IS NULL


select Name, Gender, Salary, DepartmentName
FROM tbl_Employees
FULL JOIN tbl_Department
ON tbl_Employees.DepartmentID = tbl_Department.ID
where tbl_Employees.DepartmentID IS NULL
OR tbl_Department.ID IS NULL







