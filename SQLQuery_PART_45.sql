--Instead of insert triggers Part 45

CREATE TABLE tblEmployee45
(
  Id int Primary Key,
  Name nvarchar(30),
  Salary int,
  Gender nvarchar(10),
  DepartmentId int
)


Insert into tblEmployee45 values (1,'John', 5000, 'Male', 3)
Insert into tblEmployee45 values (2,'Mike', 3400, 'Male', 2)
Insert into tblEmployee45 values (3,'Pam', 6000, 'Female', 1)
Insert into tblEmployee45 values (4,'Todd', 4800, 'Male', 4)  
Insert into tblEmployee45 values (5,'Sara', 3200, 'Female', 1) 
Insert into tblEmployee45 values (6,'Ben', 4800, 'Female', 3) 

CREATE TABLE tblDepartment45
(
  DeptId int identity(1,1) primary key,
  DepartmentName nvarchar(1000)
)

Insert into tblDepartment45 values ('IT')
Insert into tblDepartment45 values ('Payroll')
Insert into tblDepartment45 values ('HR')
Insert into tblDepartment45 values ('Admin')

select * from tblEmployee45 
select * from tblDepartment45

ALTER TABLE tblEmployee45 add constraint FK_tblEmployee45_DepartmentId
FOREIGN KEY (DepartmentId) references tblDepartment45(DeptId)

   

Create View VWEmployeesDetails45
as
select Id, Name, Gender, DepartmentName
from tblEmployee45
JOIN tblDepartment45
ON tblDepartment45.DeptId = tblEmployee45.DepartmentId

select * from VWEmployeesDetails45

insert into VWEmployeesDetails45 values(7,'Valarie','Female','IT')

CREATE trigger tr_VWEmployeesDetails45_InsteadofInsert
ON VWEmployeesDetails45
INSTEAD of Insert
as
Begin
	select * from inserted
	select * from deleted
End

insert into VWEmployeesDetails45 values(8,'Valarie','Female','IT')

--------------------------------------------------------------------

ALTER TRIGGER Tr_VWEmployeesDetails45_InsteadofInsert
ON VWEmployeesDetails45
Instead of insert 
as
Begin
		Declare @DepartId int

		--Check if there is a valid DepartmentID
		--For the given DepartmentsName
		
		select @DepartId = DeptId
		from tblDepartment45
		JOIN inserted
		ON inserted.DepartmentName = tblDepartment45.DepartmentName

		--if DepartmentId is null throw an error
		--and stop processing

		if(@DepartId is null)
		Begin
			Raiserror('Invalid Department Name. Statement terminated', 16, 1)
			return
		End

		--Finally insert into tblEmployees table

		insert into tblEmployee45(Id, Name,Gender, DepartmentId)
		select ID, Name, Gender, @DepartId
		from inserted
End

----------------------------------------------------------

insert into VWEmployeesDetails45 values(9,'Valarie','Female','IdfdfT')

insert into VWEmployeesDetails values(9,'Valarie','Female','IT')







