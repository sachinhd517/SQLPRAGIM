-- BUILT IN STRING FUNCTION IN SQL SERVER 2008 PART - 21

-- String Functions ASCII AND CHAR

--1. ASCILL(Charracter_Expression) : Return the ASCII Code of the given character expression
--2. CHAR(Interger_Expression)	   : Convert an int ASCII Code to a character. The Integer_Expression,
--								     should be between 0 to 255
--3. LTRIM(Character_Expression)     : Romoves blanks on the left handside of the given character expression.
--4. RTRIM(Character_Expression)	   : Romoves blanks on the right handside of given character expression.
--5. LOWER(Character_Expression)	   : Convert all the chareacter in the given side of the given chareacter expression, tolowercasen letters.
--6. UPPER(Chareacter_Expression)    : Convert all the chareacters in the given character _Expression, to Uppercase letters.
--7. REVERSE('Any_String_Expression'): Resverses all the chareecter in the given string expression.
--8. LEN(String_Expression)	       : Returns the count of total characters, in the given string expression, excliding the blanks at the end of the expression.
								   	

select  ASCII('a')as [ASCII VALUE]

select CHAR('a')


select CHAR (65)

Declare @Start int
Set @Start = 65
While(@Start <= 90)
Begin
	Print CHAR(@Start)
	Set @Start = @Start + 1
End

Declare @Numbers int
set @Numbers = 1
while(@Numbers <= 254)
Begin
	Print CHAR(@Numbers)
	set @Numbers = @Numbers + 1
END

Declare @Start int
Set @Start = 97
While(@Start <= 122)
Begin 
	Print CHAR(@Start)
	Set @Start = @Start + 1
End

select CHAR(123)

Declare @Start int
Set @Start = 123
While(@Start <= 150)
Begin 
	Print CHAR(@Start)
	Set @Start = @Start + 1
End

select ASCII(0)

Declare @Start int
Set @Start = 48
While(@Start <= 57)
Begin 
	Print CHAR(@Start)
	Set @Start = @Start + 1
End

select CHAR(0)
select CHAR(255)


Declare @Start int
Set @Start = 0
While(@Start <= 255)
Begin 
	Print CHAR(@Start)
	Set @Start = @Start + 1
End

select ('     Hello')

select LTRIM('     Hello')

select ('Hello         ')

select RTRIM('Hello         ')

select REVERSE('Hellow')

select UPPER('Hellow')


select LOWER('HELLOW')


select LEN('Hellow') as [Total Numbers of Chareacters]

select LEN('SACHIN HEDAOO') as [Length Of the string]

select UPPER('sachin hedaoo')as [Converting Upper case string]

select SPACE(12)as Manager