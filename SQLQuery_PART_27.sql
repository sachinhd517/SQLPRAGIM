-- DATEPART, DATEADD, DATEDIFF FUNCTION IN SQL SERVER PART - 27

--DatePart(DatePart, Date) - Returns as integer reppresenting the specified Datepart. This function is simialar to DateName(). DateName() returns
--nvarchar, where as DatePart() return an integer.

--DATEADD(datepart, NumberToAdd, date) - Returns the DateTime, after
--adding sepecified NumberToAdd, to the datepart specified of the given
--date.

--DATEDIFF(datepart, startdate, enddate) - Return the count of the sepcified datepart boundries crossed between the specified startdate
--and enddate.

