--SQL SERVER PART 12

use [ASP_DATABASE]
GO

CREATE TABLE tbl_Employees
(
	[ID] int IDENTITY PRIMARY KEY NOT NULL,
	[Name] nvarchar(50) NOT NULL,
	[Gender] nvarchar(50) NOT NULL,
	[Salary] nvarchar(50) NOT NULL,
	[DepartmentID] int
)
select * from tbl_Employees
insert into tbl_Employees values('Tom','Male',4000,1)

CREATE TABLE tbl_Department
(
	[ID] int IDENTITY PRIMARY KEY NOT NULL,
	[DepartmentName] nvarchar(50) NOT NULL,
	[Location] nvarchar(50) NOT NULL,
	[DepartmentHead] nvarchar(50) NOT NULL
)
select * from tbl_Department
select * from tbl_Employees

----------------------------------------------
--JOIN QUERY

select Name,Gender,Salary,DepartmentName
from tbl_Employees
JOIN tbl_Department
ON tbl_Employees.DepartmentID = tbl_Department.ID

----------------------------------------------------
--INNER JOIN QUERY

select Name,Gender,Salary,DepartmentName
from tbl_Employees
INNER JOIN tbl_Department
ON tbl_Employees.DepartmentID = tbl_Department.ID

-----------------------------------------------------
--LEFT JOIN QUERY, OUTER KEYWORD OPTIONAL

select Name,Gender,Salary,DepartmentName
from tbl_Employees
LEFT OUTER JOIN tbl_Department
ON tbl_Employees.DepartmentID = tbl_Department.ID

-----------------------------------------------------
--RIGHT OUTER JOIN, OUTER KEYWORD OPTIONAL

select Name,Gender,Salary,DepartmentName
from tbl_Employees
RIGHT OUTER JOIN tbl_Department
ON tbl_Employees.DepartmentID = tbl_Department.ID

-----------------------------------------------------
-- FULL OUTER JOIN, OUTER KEYWORD IS OPTIONAL

select Name,Gender,Salary,DepartmentName
from tbl_Employees
FULL OUTER JOIN tbl_Department
ON tbl_Employees.DepartmentID = tbl_Employees.ID

-----------------------------------------------------
-- CROSS JOIN 

 select Name,Gender,Salary,DepartmentName
 from tbl_Employees
 CROSS JOIN tbl_Department

 ---------------------------------------------------
-- JOIN SYNTAX

SELECT [COLUMN_LIST]
FROM  [TABLE_NAME]
JOIN CONDITION [RIGHT TABLE]
ON [JOIN_CONDITION]

