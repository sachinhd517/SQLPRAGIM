--SCALAR USER DEFINE FUNCTION IN SQL SERVER PART - 30

--User Define Functions
--Types of User Defines Functions
--Creating a Scalar User Defined Function
--Calling a Scalar User Defined Function
--Places where we can use Scalar User Defined Function
--Altering and Dropping User Defined Function

--From Part 22 to 29, we have leant how to use many of the system function that are available in
--sql Server. In this session, we will turn our attention, to creating user defined function. In short
--UDF.

--In SQL Server there are 3 types of user defined functions
--1.	Scalar functions
--2.	Inline table-valued functions
--3.	Mult-statement table-valued functions

--Scalar function may or may not have parameters, but always return a single(scalar) value. The
--retunred value can be of any data type, except text, ntext, image, cursor, and timestamp

--SYETAX

--CREATE FUNCTION Function_Name(@Parameter1 DataType,@Parameter1 DataType,@Parameter1 DataType,@Parameter_numberof DataType)
--RETURN Return_DataType
--AS 
--BEGIN
--		--Function Body
--		Return Return_DataType
--END

--A store procedure also can accept DateOfBirth and return Age, but you cannot use stroed
--procedure in a select or where clause. This is just one difference betweenn a function and a
--stored procedure. There are several other differences, which we will talk about in a later
--session.

--To after a fuction we use ALTER FUNCTION FunctionName statement and to delete it, we use


DROP FUNCTION FuncationName


DECLARE @DOB DATE
DECLARE @Age INT
SET @DOB = '07/02/1990'

SET @Age = DATEDIFF(YEAR, @DOB, GETDATE()) -
	CASE
	WHEN (MONTH(@DOB) > MONTH(GETDATE())) OR 
		 (MONTH(@DOB)= MONTH(GETDATE()) AND DAY(@DOB)> DAY(GETDATE()))
		 THEN 1
		 ELSE 0
		END 
select @Age
-----------------------------------------------------------------------

CREATE FUNCTION CalculateAge (@DOB Date)
RETURNS INT
AS 
BEGIN
DECLARE @Age INT

SET @Age = DATEDIFF(YEAR, @DOB, GETDATE()) -
	CASE
	WHEN (MONTH(@DOB) > MONTH(GETDATE())) OR 
		 (MONTH(@DOB)= MONTH(GETDATE()) AND DAY(@DOB)> DAY(GETDATE()))
		 THEN 1
		 ELSE 0
		END 
RETURN @Age
END
---------------------------------------------------------------------
select CalculateAge('07/02/1990') --wrong query

select dbo.CalculateAge('07/02/1990') AS Age -- correct query

select ASP_DATABASE.dbo.CalculateAge('07/02/1990') AS Age

select * from tblDateofBirthEmp

select ID,Name,dbo.CalculateAge(DateOfBirth) as Age from tblDateofBirthEmp
where DateOfBirth>30
   
sp_help CalculateAge

sp_helptext   CalculateAge



CREATE PROCEDURE spCalculateAge 
@DOB Date
AS 
BEGIN
DECLARE @Age INT

SET @Age = DATEDIFF(YEAR, @DOB, GETDATE()) -
	CASE
	WHEN (MONTH(@DOB) > MONTH(GETDATE())) OR 
		 (MONTH(@DOB)= MONTH(GETDATE()) AND DAY(@DOB)> DAY(GETDATE()))
		 THEN 1
		 ELSE 0
		END 
SELECT @Age
END

EXECUTE spCalculateAge '07/02/1990' 
--------------------------------------------------------------------------------

