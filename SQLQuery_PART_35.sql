--Why INdexes

--Indexes are used by queries to find data from tables quickly. Indexes are created on
--tables and views. Index on a table or a view, is very similar to an index that we find in a 
--book.

--If you don't have an index, and i ask you to locate a specific chapter in the book, you will 
--have to look at every page starting from the first of the book.

--On, the other hand, if you jave the index, you lookup the page number of the cha[ter in
--the index, and then directly go to that page number to locate the chapter.

--Obviously, the book index is helping to drastically reduce the time it takes to find the
--chapter.

--In a similar way, Table and View indexes, can help the query to find data quickly.

--In fact, the existence of the right indexes, can dreastically improve the performance of the
--query. If there is no index to help the query, then the query engine, checks every row in
--the table from the beginning to the end. This is called as Table Scan. Table scan is bad
--for performance.

 Create Table tbl_Employee35
 (
	[ID] int PRIMARY KEY NOT NULL,
	[Name] nvarchar(50) NOT NULL,
	[Salary] nvarchar(50) NOT NULL,
	[Gender] nvarchar(50) NOT NULL
 )

 insert into tbl_Employee35 values(1,'Sam',2500,'Male')
 
 insert into tbl_Employee35 values(2,'Pam',6500,'Female')
 
 insert into tbl_Employee35 values(3,'John',4500,'Male')
 
 insert into tbl_Employee35 values(4,'Sara',5500,'Female')

 insert into tbl_Employee35 values(5,'Todd',3100,'Male')

 select * from tbl_Employee35
 


CREATE Index IX_tbl_Employee35_Salary
ON tbl_Employee35 (Salary ASC)

sp_helpindex tbl_Employee35
 
drop index tbl_Employee35.IX_tbl_Employ_Salary

--how to create indexes in graphical view

--1.	GO to the Tables
--2.	Select table name as you created.
--3.	Click on + sign
--4.	Right Click on Index and add new index as choice Cluster and non-cluster

