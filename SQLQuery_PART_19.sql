
--Useful system Stored procedures

--sp_help procedure_name - View the information about the stored procedure,
--like parameters name, their datatypes etc. sp_help can be used with any
--database object, like tables, views, sp's, triggers etc. Alternatively, you can 
--also press ALT+F1, when the name of the object is highlighed.

--sp_helptext procedure_name - View the Text of the stored procedure

--sp_depends procedure_name - View the dependencies of the stored
--procedure. This system SP is very useful, especially if you want to check,
--if there are any stored procedures that are referencing a table 
--that you are abput to drop. sp_depends can also be used with other database objects like table etc.


--Defination

--The OUTPUT clause was introduced in SQL Server 2005 version. The OUTPUT clause returns the values of each 
--row that was affected by an INSERT, UPDATE or DELETE statements. ... The result from the OUTPUT clause can 
--be inserted into a separate table during the execution of the query.


create table tblEmployee19
(
	[Id] int primary key NOT NULL,
	[Name] nvarchar(50) NOT NULL,
	[Gender] nvarchar(50) NOT NULL,
	[DepartmentId] int NOT NULL
)
Create table tblDepartment19
(
	[ID] int primary key,
	[Dept_Name] nvarchar(50)
)


select * from tblEmployee19
select * from tblDepartment19

delete  tblEmployee19
delete tblDepartment19

Alter table tblEmployee19 add constraint FK_tblEmployee19_GenderId
FOREIGN KEY (DepartmentId) references tblDepartment19(ID)

insert into tblEmployee19 values(1,'sam','Male',1)
insert into tblEmployee19 values(2,'Ram','Male',1)
insert into tblEmployee19 values(3,'Sara','Female',3)
insert into tblEmployee19 values(4,'Todd','Male',2)
insert into tblEmployee19 values(5,'John','Male',3)
insert into tblEmployee19 values(6,'Sana','Female',2)
insert into tblEmployee19 values(7,'James','Male',1)
insert into tblEmployee19 values(8,'Rob','Male',2)
insert into tblEmployee19 values(9,'Steve','Male',1)
insert into tblEmployee19 values(10,'Pam','Female',2)


insert into tblDepartment19 values(1,'Devloper')
insert into tblDepartment19 values(2,'HR')
insert into tblDepartment19 values(3,'Account')


Alter table tblEmployee19 add constraint FK_tblEmployee19_GenderId
Foreign key (DepartmentId) references tblDepartment19(ID)


create proc spGetEmployeeCountByGender19
@Gender nvarchar(50),
@EmployeeCount int OUTPUT
as
Begin
	select @EmployeeCount = COUNT(Id) from tblEmployee19 where Gender = @Gender
END

Declare @TotalCount int
EXECUTE spGetEmployeeCountByGender19 'Male', @TotalCount Output
if(@TotalCount is NULL)

	print	'@TotalCount is null'

else

	print	'@TotalCount is not null'

Print @TotalCount

sp_help spGetEmployeeCountByGender19

sp_depends spGetEmployeeCountByGender19

sp_depends tblDepartment19

CREATE PROCEDURE spGetEmployeeCountByGender
@Gender nvarchar(20),
@EmployeeCount int OUTPUT
as
Begin
	select COUNT(*) from tblEmployee19 where Gender = @Gender
END

--HOW TO EXECUTE INPUT and OUTPUT PARAMETER QUERY

Declare @TotalCount int
Execute spGetEmployeeCountByGender19 'Male', @TotalCount OUTPUT
Print @TotalCount

Declare @TotalCount int
Execute spGetEmployeeCountByGender 'Male', @TotalCount 
Print @TotalCount


Declare @TotalCount int
Execute spGetEmployeeCountByGender 'Male', @TotalCount 
if(@TotalCount is null)
	print '@TotalCount is null'
else
	print '@TotalCount is not null'
Print @TotalCount

Declare @TotalCount int
Execute spGetEmployeeCountByGender19  @EmployeeCount = @TotalCount OUTPUT, @Gender = 'Male'
Print @TotalCount


sp_help spGetEmployeeCountByGender
sp_help tblEmployee18
sp_helptext spGetEmployeeCountByGender
sp_depends tblEmployee18