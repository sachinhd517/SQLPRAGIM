--instead of delete triggers in sql server PART - 47

--Triggers 

--In SQL server there are 3 types of triggers

--1.	DML triggers
--2.	DDL triggers
--3.	Logon triggers

--DML trigger are fired automatically in response to DML events(INSERT,UPDATE & DELETE)

--DML triggers can be again classified into 2 types

--1.	After triggers (Something called as FOR triggers)
--2.	Instead of triggers

--After triggers, fires after the triggers action.
--(INSERT,UPDATE and DELETE)


CREATE TABLE tblEmployee47
(
  Id int Primary Key,
  Name nvarchar(30),
  Salary int,
  Gender nvarchar(10),
  DepartmentId int
)


Insert into tblEmployee47 values (1,'John', 5000, 'Male', 3)
Insert into tblEmployee47 values (2,'Mike', 3400, 'Male', 2)
Insert into tblEmployee47 values (3,'Pam', 6000, 'Female', 1)
Insert into tblEmployee47 values (4,'Todd', 4800, 'Male', 4)  
Insert into tblEmployee47 values (5,'Sara', 3200, 'Female', 1) 
Insert into tblEmployee47 values (6,'Ben', 4800, 'Female', 3) 

CREATE TABLE tblDepartment47
(
  DeptId int identity(1,1) primary key,
  DepartmentName nvarchar(1000)
)

Insert into tblDepartment47 values ('IT')
Insert into tblDepartment47 values ('Payroll')
Insert into tblDepartment47 values ('HR')
Insert into tblDepartment47 values ('Admin')

select * from tblEmployee47 
select * from tblDepartment47

ALTER TABLE tblEmployee47 add constraint FK_tblEmployee47_DepartmentId
FOREIGN KEY (DepartmentId) references tblDepartment47(DeptId)


CREATE View VWEmpolyeeDetails47
as
select ID, Name, Gender, DepartmentName
from tblEmployee47
JOIN tblDepartment47
ON tblEmployee47.DepartmentId = tblDepartment47.DeptId

select * from VWEmpolyeeDetails47

delete from VWEmpolyeeDetails47 where ID = 1


Create Trigger tr_VWEmployeeDetails_InseteadOfDelete47
on VWEmpolyeeDetails47
instead of delete
as
Begin
	
		Delete tblEmployee47
		from tblEmployee47
		JOIN deleted 
		ON tblEmployee47.Id = deleted.Id

		--Subquery
		--Delete from tbl_NewEmployee
		--Where ID in(select ID from deleted.)
END


 Delete from VWEmpolyeeDetails47 where ID = 1

 ------------------------------------------------------------
 
CREATE Trigger tr_VWEmployee_InseteadOfDelete_UsingSubquery
on VWEmpolyeeDetails47
instead of delete
as
Begin
	
		--Delete tbl_NewEmployee
		--from tbl_NewEmployee
		--JOIN deleted 
		--ON tbl_NewEmployee.ID = deleted.ID

		--Subquery
		Delete from tblEmployee47
		Where Id in(select Id from deleted)
END

delete from VWEmpolyeeDetails47 where ID IN (3,4)




