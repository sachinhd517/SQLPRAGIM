--Part 11

--Group by clause is used to group a selected set of rows into a set
--of summary rows by the values of one or more column or esxpressions. 
--it is alwasy used in conjunction with one or more aggregate functions.

--Note :  If you omit, the GROUP BY clause and try to execute the query, you get an error
--column 'tblEmployee.city' is invalud in the select list because it is not contained in either an
--aggregate function or the GROUP BY clause.

--Filtering Groups: 

--WHERE clause is used to filter rows before aggregation, where as HAVING clause is used to
--filter group after aggregations. The following 2 queries produce the same result.

--Note : Form a performance standpoint, you cannot say that one method is less efficient than
--the other. Sql server optimizer analyse each statment and selects an efficient way of
--executing it. As a best practice, use the syntax that clearly describes the desired result. Try
--to eliminate rows that you wouldn't need, as early as possible.

						--Difference - WHERE and Having

--1.  WHERE clause can be used with - Select, Insert, and Update statements,
--	  where as HAVING clause can only be used with the select statement.
--2.  WHERE filter rows before aggregation(GROUPING), where as, HAVING
--	  filter groups, after the aggregations are performed.
--3.  Aggregate functions cannot be used in the WHERE clause, unless it is in
--	  a sub query contained in a HAVING clause, whereas, aggregate functions
--	  can be used in Having clause.



Create table tblEmployeePart11
(
	[ID] int primary key NOT NULL,
	[Name] nvarchar(50) NOT NULL,
	[Gender] nvarchar(50) NOT NULL,
	[Salary] int NOT NULL,
	[City] nvarchar(50) NOT NULL
)

select * from tblEmployeePart11

Drop table tblEmployeePart11

truncate table tblEmployeePart11

insert into tblEmployeePart11 values(1,'Tom','Male',4000,'London')
insert into tblEmployeePart11 values(2,'Pam','Female',3000,'New York')
insert into tblEmployeePart11 values(3,'John','Male',3500,'London')
insert into tblEmployeePart11 values(4,'Sam','Male',4500,'London')
insert into tblEmployeePart11 values(5,'Todd','Male',2800,'Sydney')
insert into tblEmployeePart11 values(6,'Ben','Male',7000,'New York')
insert into tblEmployeePart11 values(7,'Sara','Female',4800,'Sydney')
insert into tblEmployeePart11 values(8,'Valari','Female',5500,'New York')
insert into tblEmployeePart11 values(9,'James','Male',6500,'London')
insert into tblEmployeePart11 values(10,'Russell','Male',8800,'London')

select * from tblEmployeePart11

select MAX(Salary) from tblEmployeePart11
select SUM(Salary) from tblEmployeePart11
select MIN(Salary) from tblEmployeePart11

select City, SUM(Salary) as TotalSalary
from tblEmployeePart11
Group by City

select City, SUM(Salary) as TotalSalary
from tblEmployeePart11
Group by City


select City, Gender, SUM(Salary) as TotalSalary
from tblEmployeePart11
Group by City,Gender
Order by


select City, Gender, SUM(Salary) as TotalSalary
from   tblEmployeePart11
Group by City,Gender
Order by desc

select Gender, City, SUM(Salary) as TotalSalary
from tblEmployeePart11
Group by Gender, City

select COUNT(*) from tblEmployeePart11

select Gender, City, SUM(Salary) as TotalSalary, COUNT(ID) as [Total Employees]
from tblEmployeePart11
Group by Gender, City


select Gender, CIty, SUM(Salary) as [Total Salary], COUNT(ID) as [TOtal Employees]
from tblEmployeePart11
where Gender = 'Male'
group by Gender, City


select Gender, CIty, SUM(Salary) as [Total Salary], COUNT(ID) as [TOtal Employees]
from tblEmployeePart11
group by Gender, City
Having Gender = 'Male'

