--MERGE IN SQL SERVER

--What IS the USE MERGE statement IN SQL SERVER
--MERGE statement introduced IN SQL SERVER 2008 allows us TO perofrom inserts,
--UPDATE AND Delate IN one STATEMENT. THis means we NO longer haev TO USE
--multiple statements FOR performing INSERT, UPDATE AND Delate

--WITH MERGE statment we require 2 tables
--1. SOURCE TABLE - Constinas the CHANGES that needs TO be applied TO the TARGET TABLE
--2. TARGET TABLE - The TABLE that require CHANGES(INSERT,UPDATE AND deletes)
 


USE[SQLSERVERPRAGIM]
GO


CREATE TABLE StudentSource69
(
    ID int PRIMARY KEY,
    Name NVARCHAR(50)
)

Insert into StudentSource69 values (1, 'Mike')
Insert into StudentSource69 values (2, 'Sara')

create TABLE StudentTarget69
(
    ID int PRIMARY KEY,
    Name NVARCHAR(50)
)
Insert into StudentTarget69 values (1, 'Mike M')
Insert into StudentTarget69 values (3, 'John')

select * from StudentSource69
SELECT * FROM StudentTarget69

MERGE INTO StudentTarget69 AS T
USING StudentSource69 AS S
ON T.ID = S.ID
WHEN MATCHED THEN
    UPDATE SET T.NAME = S.NAME
WHEN NOT MATCHED BY TARGET THEN
    INSERT (ID, Name) VALUES(S.ID,S.Name)
WHEN NOT MATCHED BY SOURCE THEN
    DELETE;


TRUNCATE TABLE StudentSource69
TRUNCATE TABLE StudentTarget69


MERGE INTO StudentTarget69 AS T
USING StudentSource69 AS S
ON T.ID = S.ID
WHEN MATCHED THEN
    UPDATE SET T.NAME = S.NAME
WHEN NOT MATCHED BY TARGET THEN
    INSERT (ID, Name) VALUES(S.ID,S.Name);


