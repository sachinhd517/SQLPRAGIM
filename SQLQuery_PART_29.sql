--MATHETICAL EXPRESION IN SQL SERVER PART - 29

--Mathematical function
--Abs
--Ceiling
--Floor
--Power
--Rand
--Square
--Sqrt
--Round

--ABS(numeric_expression) - ABS stands for absolute and return, the absolute(positive) number.

--CEILING(numeric_expression) and FLOOT(numeric_expression)

--CEILING and FLOOR function accept a numeric expression as a single paramter. CEILING() return
--the smallest integer value greater than or equal to the parameter, whereas FLOOR() returns the 
--largest integer less than or equal to the parameter

--Power(expression,power)
--Return the power value of the specified expression to the psecified power.

--SQUARE(Number)
--Returns the square of the given number

--SQRT(Number)
--Return the Square root of the given number

--RAND([Seed_Value]) - Returns a random float number between 0 and 1 Rand() function takes
--an optional seed parameter. When seed value is suppiled the RAND() function always return the 
--same value for the same seed.

--ROUND() FUNCTION

--ROUND(numeric_expression,length[,function]) - Round the given numeric expression based on
--the given length. This function takes 3 parameters.

--1.	Numeric_Expression is the number that we want to road.
--2.	Length parameter, specifies the number of the digits that we want to round to. If the lenght is
--	positive number, then the rounding is applied for the decimal part, where as if the lenght is
--	negative, then the rounding is applied to the number before the decimal
--3.	The optional function parameter, is used to indicate rounding or truncation opertation. 0 
--	indicates rounding non zer indicates truncation. Default, if not specified is 0.

select ABS(-123.9) 

select CEILING(15.2)
select CEILING(-15.2)

select FLOOR(89.9)
select FLOOR(-89.9)

select POWER(12,2)
select POWER(2,3)

select SQUARE(2)as SQUARE

select SQRT(2)

select RAND(1)
select RAND()

select (RAND() * 100)
select FLOOR(RAND() * 100)

Declare @Counter INT
Set @Counter = 1
while(@Counter <= 10)
Begin
	Print FLOOR(RAND() * 100)
	Set @COunter = @Counter + 1
End

Declare @Counter INT
Set @Counter = 1
while(@Counter <= 10)
Begin
	Print FLOOR(RAND() * 1000)
	Set @COunter = @Counter + 1
End

select ROUND(855.556, 2)
select ROUND(855.556, 2,1)
select ROUND(855.556, 1)
select ROUND(855.556, 2)
select ROUND(855.556, 1,1)
select ROUND(855.556, -2)
select ROUND(855.556, -1)





