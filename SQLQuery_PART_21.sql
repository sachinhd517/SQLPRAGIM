-- ADVANTAGE OF STORED PROCEDURES PART - 21

--   Advantages of stored procedures

 -- 1. Execution plan retendtion and resuability
 -- 2. Reduces network traffic
 -- 3. Code reusability and better maintainability
 -- 4. Better Security
 -- 5. Avoid SQL Injection attack 


Create table tblEmp21
(
	[ID] int PRIMARY KEY IDENTITY NOT NULL,
	[Name] nvarchar(50) NOT NULL,
	[Gender] nvarchar(50) NOT NULL  
)

insert into tblEmp21 values('Sara','Female')
insert into tblEmp21 values('John','Male')
insert into tblEmp21 values('Pam','Female')

select * from tblEmp21

ALTER PROCEDURE SPGetNameByID21 
@ID int
WITH ENCRYPTION
as
Begin
		select Name  from tblEmp21 where ID = @ID
END

EXECUTE SPGetNameByID21 1

EXECUTE SPGetNameByID21 2


select Name From tblEmp21 where ID = 1
