select * from Employees
select * from JOBS
select * from LOCATIONS
select * from DEPARTMENTS

create table JOBS(JOB_ID varchar(10),JOB_TITLE varchar(35),MIN_SALARY int,MAX_SALARY int)
insert into JOBS values('AD_PRES','President',20000,40000);
insert into JOBS values('AD_VP','Administration Vice President',15000,30000);
insert into JOBS values('AD_ASST','Administration Assistant',3000,6000);
insert into JOBS values('FI_MGR','Finance Manager',8200,16000);
insert into JOBS values('FI_ACCOUNT','Accountant',4200,9000);
insert into JOBS values('AC_MGR','Accounting Manager',8200,16000);
insert into JOBS values('AC_ACCOUNT','Public Accountant',4200,9000);
insert into JOBS values('SA_MAN','Sales Manager',10000,20000);
insert into JOBS values('SA_REP','Sales Representative',6000,12000);
insert into JOBS values('PU_MAN','Purchasing Manager',8000,15000);
insert into JOBS values('PU_CLERK','Purchasing Clerk',2500,5500);
insert into JOBS values('ST_MAN','Stock Manager',5500,8500);
insert into JOBS values('ST_CLERK','Stock Clerk',2000,5000);
insert into JOBS values('SH_CLERK','Shipping Clerk',2500,5500);
insert into JOBS values('IT_PROG','Programmer',4000,10000);
insert into JOBS values('MK_MAN','Marketing Manager',9000,15000);
insert into JOBS values('MK_REP','Marketing Representative',4000,9000);
insert into JOBS values('HR_REP','Human Resources Representative',4000,9000);
insert into JOBS values('PR_REP','Public Relations Representative',4500,10500);
 
create table DEPARTMENTS(DEPARTMENT_ID int,DEPARTMENT_NAME VARCHAR(30),MANAGER_ID int,LOCATION_ID int)
insert into DEPARTMENTS values(10,'Administration',200,1700);
insert into DEPARTMENTS values(20,'Marketing',201,1800);
insert into DEPARTMENTS values(30,'Purchasing',114,1700);
insert into DEPARTMENTS values(40,'Human Resources',203,2400);
insert into DEPARTMENTS values(50,'Shipping',121,1500);
insert into DEPARTMENTS values(60,'IT',103,1400);
insert into DEPARTMENTS values(70,'Public Relations',204,2700);
insert into DEPARTMENTS values(80,'Sales',145,2500);
insert into DEPARTMENTS values(90,'Executive',100,1700);
insert into DEPARTMENTS values(100,'Finance',108,1700);
insert into DEPARTMENTS values(110,'Accounting',205,1700);
insert into DEPARTMENTS values(120,'Treasury',NULL,1700);
insert into DEPARTMENTS values(130,'Corporate Tax',NULL,1700);
insert into DEPARTMENTS values(140,'Control And Credit',NULL,1700);
insert into DEPARTMENTS values(150,'Shareholder Services',NULL,1700);
insert into DEPARTMENTS values(160,'Benefits',NULL,1700);
insert into DEPARTMENTS values(170,'Manufacturing',NULL,1700);
insert into DEPARTMENTS values(180,'Construction',NULL,1700);
insert into DEPARTMENTS values(190,'Contracting',NULL,1700);
insert into DEPARTMENTS values(200,'Operations',NULL,1700);
insert into DEPARTMENTS values(210,'IT Support',NULL,1700);
insert into DEPARTMENTS values(220,'NOC',NULL,1700);
insert into DEPARTMENTS values(230,'IT Helpdesk',NULL,1700);
insert into DEPARTMENTS values(240,'Government Sales',NULL,1700);
insert into DEPARTMENTS values(250,'Retail Sales',NULL,1700);
insert into DEPARTMENTS values(260,'Recruiting',NULL,1700);
insert into DEPARTMENTS values(270,'Payroll',NULL,1700);

create table LOCATIONS(LOCATION_ID int,STREET_ADDRESS varchar(40),
 POSTAL_CODE varchar(12),	CITY varchar(30),	STATE_PROVINCE varchar(25),
COUNTRY_ID char(2))
insert into LOCATIONS values(1000,'1297 Via Cola di Rie','00989','Roma',NULL,'IT');
insert into LOCATIONS values(1100,'93091 Calle della Testa','10934','Venice',NULL,'IT');
insert into LOCATIONS values(1200,'2017 Shinjuku-ku','1689','Tokyo','Tokyo Prefecture','JP');
insert into LOCATIONS values(1300,'9450 Kamiya-cho','6823','Hiroshima',NULL,'JP');
insert into LOCATIONS values(1400,'2014 Jabberwocky Rd','26192','Southlake','Texas','US');
insert into LOCATIONS values(1500,'2011 Interiors Blvd','99236','South San Francisco','California','US');
insert into LOCATIONS values(1600,'2007 Zagora St','50090','South Brunswick','New Jersey','US');
insert into LOCATIONS values(1700,'2004 Charade Rd','98199','Seattle','Washington','US');
insert into LOCATIONS values(1800,'147 Spadina Ave','M5V 2L7','Toronto','Ontario','CA');
insert into LOCATIONS values(1900,'6092 Boxwood St','YSW 9T2','Whitehorse','Yukon','CA');
insert into LOCATIONS values(2000,'40-5-12 Laogianggen','190518','Beijing',NULL,'CN');
insert into LOCATIONS values(2100,'1298 Vileparle (E)','490231','Bombay','Maharashtra','IN');
insert into LOCATIONS values(2200,'12-98 Victoria Street','2901','Sydney','New South Wales','AU');
insert into LOCATIONS values(2300,'198 Clementi North','540198','Singapore',NULL,'SG');
insert into LOCATIONS values(2400,'8204 Arthur St',Null,'London',null,'UK');
insert into LOCATIONS values(2500,'Magdalen Centre, The Oxford Science Park','OX9 9ZB','Oxford','Oxford','UK');
insert into LOCATIONS values(2600,'9702 Chester Road','09629850293','Stretford','Manchester','UK');
insert into LOCATIONS values(2700,'Schwanthalerstr. 7031','80925','Munich','Bavaria','DE');
insert into LOCATIONS values(2800,'Rua Frei Caneca 1360','01307-002','Sao Paulo','Sao Paulo','BR');
insert into LOCATIONS values(2900,'20 Rue des Corps-Saints','1730','Geneva','Geneve','CH');
insert into LOCATIONS values(3000,'Murtenstrasse 921','3095','Bern','BE','CH');
insert into LOCATIONS values(3100,'Pieter Breughelstraat 837','3029SK','Utrecht','Utrecht','NL');
insert into LOCATIONS values(3200,'Mariano Escobedo 9991','11932','Mexico City','Distrito Federal','MX');
commit;
