--What is the use of Javascript in ASP.NET

CREATE TABLE tblUser
(
	 [ID] int Primary key IDENTITY,
	 [FirstName] nvarchar(50),
	 [LastName] nvarchar(50),
	 [Email] nvarchar(50) 
) 

CREATE Procedure spInsertUser 
@FirstName nvarchar(50),
@LastName nvarchar(50),
@Email nvarchar(50)
as
Begin
		Insert into tblUser values(@FirstName,@LastName,@Email)
End
GO

select * from tblUser