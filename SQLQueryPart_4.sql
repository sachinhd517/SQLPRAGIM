--PART 4

--A column default can be specified using Default constraint. The DEFAULT
--consraint is used to insert a default value into a column. The default
--value into a column. The default value will be added to all new records, 
--if no other value is specified, including NULL

--Altering an existring column to add a default constraint:

--SYNTAX

--ALTER TABLE [TABLE_NAME]
--ADD CONSTRAINT {CONSTRAINT_NAME}
--DEFAULT {DEFAULT_VALUE} FOR {EXISTING_COLUMN_NAME}

--Adding a new column, with default value, to an existing table:

--ALTER TABLE {TABLE_NAME}
--ADD {COLUMN_NAME} {DATA_TYPE}{ NULL | NOT NULL}
--CONSTRAINT {CONSTRAINT_NAME} DEFAULT {DEFAULT_VALUE}

--Dropping a constraint :

--ALTER TABLE {TABLE_NAME}
--DROP CONSTRAINT {CONSTRAINT_NAME}


use [SQLSERVERPRAGIM]
GO


CREATE TABLE tblPerson4
(
	[ID] int primary key NOT NULL,
	[Name] nvarchar(50),
	[Email] nvarchar(50),
	[GenderID] int 
)
CREATE TABLE tblGender4
(
	[ID] int primary key NOT NULL,
	[Gender] nvarchar(50)
)

insert into tblPerson4 values (3,'ram','r@r.com',1)
insert into tblPerson4 values (4,'shyam','s@#r.com',2)
insert into tblPerson4 values (5,'Ritu','r$%r.com',2)
insert into tblPerson4 values (6,'Radhika','R@#r.com',2)
insert into tblPerson4 values (7,'Jayesh','j@#b.com',1)
insert into tblPerson4 values (8,'Sonu','S@#u.com',2)
insert into tblPerson4 values (9,'Radha','r@r.com',NULL)
insert into tblPerson4 values (10,'Rajesh','r@r.com',NULL)
insert into tblPerson4 values (11,'Varun','r@r.com',1)
insert into tblPerson4 values (12,'shesha','shesha@gmail.com',NULL)
insert into tblPerson4 values (13,'Ramu','Ramu@gmail.com',NULL)
insert into tblPerson4 values (14,'Ram','Ram@gmail.com',NULL)



insert into tblGender4 values(1,'Male')
insert into tblGender4 values(2,'Fmale')

ALTER TABLE tblPerson4 add constraint FK_tblPerson4_GenderID
FOREIGN KEY (GenderID) references tblGender4(ID)

select * from tblGender4
select * from tblPerson4

--Default constraint

ALTER TABLE tblPerson4 ADD CONSTRAINT DF_tbl_Person_GenderId
DEFAULT 3 FOR GENDERID



ALTER TABLE tbl_Person4 
ADD CONSTRAINT DF_tbl_Person_GenderID
DEFAULT 3 FOR GenderId


ALTER TABLE tbl_Person4
ADD CONSTRAINT DF_tbl_Person_GenderID
DEFAULT 3 FOR GENDERID


ALTER TABLE tblPerson4
ADD CONSTRAINT DF_tblPerson_GenderId
DEFAULT 3 FOR GENDERID


--DROP CONSTRAINTS

ALTER TABLE tblPerson4
DROP CONSTRAINT DF_tbl_Person_GenderId

ALTER TABLE tblPerson4
DROP CONSTRAINT DF_tbl_Persson_GenderId

--ADDING NEW COLUMN

ALTER TABLE tblPerson4
ADD PhoneNumber int NOT NULL
CONSTRAINT DF_tbl_Person_GenderId DEFAULT 3




