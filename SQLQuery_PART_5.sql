--Cascading referential integrity


--Cascading referential integrity constraint allows to define the action Microsoft SQL
--Server should take when a user attempts to delete or update a key to which an existing
--foreign keys poiunts.

--Options when setting up Cascading referential integrity constraint : 

--1. No Action : This is the default behaviour. No Action sepecifies that if an attempt is made to delete
--or update a row with a key referenced by foregin keys in existing rows in other tables, an error is 
--raised and the DELETE or UPDATE is rolled back

--2. Cascade : Specifies that if an attempt is made ot delete or update a row with a key referenced by 
--foreign keys in existing rows in other tables, all rows containing those foreign keys are also deleted
--or updated.

--3. Set NULL : Specifies that if an attmpt is made to delete or update a row with a key refernced
--by foreign keys in existing rows in other tables, all rows containing those foreign key are set to
--NULL.

--4. Set Default : Specifies that if an attmept is made to delete or update a tow with a key referenced
--by foreign keys in existing rows in other tables, all containing those foreign key are set to default values.


select * from tblGender
select * from tblPerson

insert into tblPerson values(15,'rama','rama@gmail.com',1)

insert into tblGender values(2,'Female')

Delete from tblGender where ID = 3

ALTER TABLE tblPerson
drop DF_tblPerson_GenderId

ALTER TABLE tblPerson add Constraint DF_tblPerson_GenderId
DEFAULT 3 FOR GenderId

ALTER TABLE tblPerson add CONSTRAINT DF_tblPerson_GenderId1
DEFAULT NULL FOR GenderId