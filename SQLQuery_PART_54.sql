--PIVOT Operator

--Pivot is a sql server opertator that can be used to trun unique values from one column, into multiple coloums
--in the ouput,there by effectively rotating a table.


Create Table tblProductSales54
(
 SalesAgent nvarchar(50),
 SalesCountry nvarchar(50),
 SalesAmount int 
)

Insert into tblProductSales54 values('Tom', 'UK', 200)
Insert into tblProductSales54 values('John', 'US', 180)
Insert into tblProductSales54 values('John', 'UK', 260)
Insert into tblProductSales54 values('David', 'India', 450)
Insert into tblProductSales54 values('Tom', 'India', 350)
Insert into tblProductSales54 values('David', 'US', 200)
Insert into tblProductSales54 values('Tom', 'US', 130)
Insert into tblProductSales54 values('John', 'India', 540)
Insert into tblProductSales54 values('John', 'UK', 120)
Insert into tblProductSales54 values('David', 'UK', 220)
Insert into tblProductSales54 values('John', 'UK', 420)
Insert into tblProductSales54 values('David', 'US', 320)
Insert into tblProductSales54 values('Tom', 'US', 340)
Insert into tblProductSales54 values('Tom', 'UK', 660)
Insert into tblProductSales54 values('John', 'India', 430)
Insert into tblProductSales54 values('David', 'India', 230)
Insert into tblProductSales54 values('David', 'India', 280)
Insert into tblProductSales54 values('Tom', 'UK', 480)
Insert into tblProductSales54 values('John', 'US', 360)
Insert into tblProductSales54 values('David', 'UK', 140) 

select * from tblProductSales54

select		SalesCountry, SalesAgent, SUM(SalesAmount) as Total
from		tblProductSales54
GROUP BY	SalesCountry, SalesAgent
ORDER BY	SalesCountry, SalesAgent

--Uery using pivot

select SalesAgent, India, US, UK from tblProductSales54
PIVOT (SUM(SalesAmount) for SalesCountry IN ([India],[US],[UK]))
as Pivatable

Create Table tblProductSales5454
(
 Id int primary key,
 SalesAgent nvarchar(50),
 SalesCountry nvarchar(50),
 SalesAmount int 
)

Insert into tblProductSales5454 values(1,'Tom', 'UK', 200)
Insert into tblProductSales5454 values(2,'John', 'US', 180)
Insert into tblProductSales5454 values(3,'John', 'UK', 260)
Insert into tblProductSales5454 values(4,'David', 'India', 450)
Insert into tblProductSales5454 values(5,'Tom', 'India', 350)
Insert into tblProductSales5454 values(6,'David', 'US', 200)
Insert into tblProductSales5454 values(7,'Tom', 'US', 130)
Insert into tblProductSales5454 values(8,'John', 'India', 540)
Insert into tblProductSales5454 values(9,'John', 'UK', 120)
Insert into tblProductSales5454 values(10,'David', 'UK', 220)
Insert into tblProductSales5454 values(11,'John', 'UK', 420)
Insert into tblProductSales5454 values(12,'David', 'US', 320)
Insert into tblProductSales5454 values(13,'Tom', 'US', 340)
Insert into tblProductSales5454 values(14,'Tom', 'UK', 660)
Insert into tblProductSales5454 values(15,'John', 'India', 430)
Insert into tblProductSales5454 values(16,'David', 'India', 230)
Insert into tblProductSales5454 values(17,'David', 'India', 280)
Insert into tblProductSales5454 values(18,'Tom', 'UK', 480)
Insert into tblProductSales5454 values(19,'John', 'US', 360)
Insert into tblProductSales5454 values(20,'David', 'UK', 140) 

select * from tblProductSales5454

select SalesAgent, India, US, UK FROM tblProductSales5454
PIVOT
(
	SUM(SalesAmount) FOR SalesCountry IN([India],[US],[UK])
)
AS PivotTable


select SalesAgent, India, US, UK
FROM
(
	select SalesAgent, SalesCountry, SalesAmount
	FROM tblProductSales5454
)as SourceTable
Pivot
(
	SUM(SalesAmount) FOR SalesCountry IN (India, US, UK)
) as PivotTable