                                       --TEANSACTION--

--a transaction is a group of commands that change the data stored in a database. 
--A transaction, is treated as a single unit. A transaction ensures that, either all of the
--commands succed, or none of them. If one of the commands in the transaction
--fails, all of the commands fail, and any data that was modifed in the databse is 
--rolled back. In this way, transaction maintain the intergrity of data in a database.

--Transaction Processing following these steps:
--1. Begin a transaction
--2. Process database commands,
--3. Check for errors.

--	if errors occurred,
--		rollback the transaction,
--	else
--		commit the transaction		

--Note: NOT able to see the un-commited changes

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED


Create Table tblProduct57
(
 ProductId int NOT NULL primary key,
 Name nvarchar(50),
 UnitPrice int,
 QtyAvailable int
)

Insert into tblProduct57 values(1, 'Laptops', 2340, 200)
Insert into tblProduct57 values(2, 'Desktops', 3467, 20)

select * from tblProduct57

Begin TRAN
Update tblProduct57 set QtyAvailable = 400 where ProductId = 1

Rollback TRAN
COMMIT TRAN


Create Table tblMailingAddress
(
   AddressId int NOT NULL primary key,
   EmployeeNumber int,
   HouseNumber nvarchar(50),
   StreetAddress nvarchar(50),
   City nvarchar(10),
   PostalCode nvarchar(50)
)

Insert into tblMailingAddress values (1, 101, '#10', 'King Street', 'Londoon', 'CR27DW') 



Create Table tblPhysicalAddress
(
 AddressId int NOT NULL primary key,
 EmployeeNumber int,
 HouseNumber nvarchar(50),
 StreetAddress nvarchar(50),
 City nvarchar(10),
 PostalCode nvarchar(50)
)

Insert into tblPhysicalAddress values (1, 101, '#10', 'King Street', 'Londoon', 'CR27DW')

select * from tblMailingAddress
select * from tblPhysicalAddress


Create Procedure spUpdateAddress
as
Begin
	Begin TRY
		Begin TRAN
			Update tblMailingAddress set City = 'LONDON'
			where AddressId = 1 and EmployeeNumber = 101

			update tblPhysicalAddress set City = 'LONDON'
			where AddressId =  1 and EmployeeNumber = 101

		commit transaction
		print 'Transaction committed'
	end TRY
	BEgin Catch
		Rollback Transaction
		print 'Transaction Rolled Back'
	End Catch
End

Create Procedure spUpdateAddressAlter
as
Begin
	Begin TRY
		Begin TRAN
			Update tblMailingAddress set City = 'LONDON1'
			where AddressId = 1 and EmployeeNumber = 101

			update tblPhysicalAddress set City = 'LONDON LONDON'
			where AddressId =  1 and EmployeeNumber = 101

		commit transaction
		print 'Transaction committed'
	end TRY
	Begin Catch
		Rollback Transaction
		print 'Transaction Rolled Back'
	End Catch
End