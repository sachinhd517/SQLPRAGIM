--We use UNIQUE constraint to enforce uniqueness of a column i.e the column shouldn't 
--allow any duplicate values. We can add a Unique constraint thru the designer or using
--a query


--To create the unique key using a query:
--Alter Table Table_Name
--Add Constraint Constraint_Name Unique(Column_Name)

--Both primary key and unique key are used to enforce, the uniquness of a column. So,
--when do you choose one over the other?
--A table can have, only one primary key. If you want to enforce uniquness on 2 or more 
--columns, then we use unique key constraint

--What is the difference between Primary kye constraint and unique key constraint?
--1.	A table can have only one primary key, but more than one unique key
--2.	Primary key does not allow nulls, where as unique key allows one null



use[SQL-SERVER-PRAGIM-DATABASE]

Create table tblEmployee9
(
	[ID] int primary key,
	[Name] nvarchar(50),
	[Email] nvarchar(50),
	[GenderId] int,
	[Age] int
)

select * from tblEmployee9

ALTER table tblEmployee9 add constraint
UQ_tblEmployee_Email Unique(Email)

insert into tblEmployee9 values(2,'xyz','a@a.com',1,20)

alter table tblEmployee
drop constraint UQ_tblEmployee_Email