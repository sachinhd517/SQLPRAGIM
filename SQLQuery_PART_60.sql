						--Correlated subquery--
--If the subquery depends on the outer query ofr its vales, then that sub query is 
--called as correlated subquery.

--In the wehre caluse of the subquery bwlow. "ProductId" column get it's value from 
--tablProducts table that is present in the outer query.

--So, here the subquery is dependent on the outer query for it's value. Hence this 
--subquery is a correlated subquery.

--Correlated subqueries get executed, once for every row that is zelected by the outer 
--query.

--Corelated subquery, cannot be executed independetly of the outer query.



Create table tblProducts60
(
	[Id] int primary key identity,
	[Name] nvarchar(50),
	[Description] nvarchar(250)
)

Create Table tblProductSales60
(
	[Id] int primary key identity,
	[ProductsId] int foreign key references tblProducts60(Id),
	[unitPrice] int,
	[QuantitySold] int
)

insert into tblProducts60 values('TV','52 inch black color LCD TV')
insert into tblProducts60 values('Laptop','Very thin blackcolor acer laptop')
insert into tblProducts60 values('Desktop','HP high performance desktop')

insert into tblProductSales60 values(3,450,5)
insert into tblProductSales60 values(2,250,7)
insert into tblProductSales60 values(3,450,4)
insert into tblProductSales60 values(3,450,9)

select * from tblProducts60
select * from tblProductSales60

select		Id, Name, Description
from		tblProducts60 where Id NOT IN(select distinct productsId from tblProductSales60)


select Name,
(select SUM(QuantitySold) from tblProductSales60 where ProductsId = tblProducts60.Id) as QtySold
from tblProducts60