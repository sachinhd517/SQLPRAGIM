----CTE

--COMMON TABLE EXPRESSION(CTE) is introduced in SQL SERVER 2005.  
--A CTE is a temporary result set, that can be referenced without a SELECT,INSERT,UPDATE or DELETE statement, that immediately follows the CTE

--CTE

--A CTE can only be referenced by a SELECT,INSERT,UPDATE, or DELETE statement,
--that immediately follows the CTC.


CREATE TABLE tblEmployee49
(
  Id int Primary Key,
  Name nvarchar(30),
  Salary int,
  Gender nvarchar(10),
  DepartmentId int
)


Insert into tblEmployee49 values (1,'John', 5000, 'Male', 3)
Insert into tblEmployee49 values (2,'Mike', 3400, 'Male', 2)
Insert into tblEmployee49 values (3,'Pam', 6000, 'Female', 1)
Insert into tblEmployee49 values (4,'Todd', 4900, 'Male', 4)  
Insert into tblEmployee49 values (5,'Sara', 3200, 'Female', 1) 
Insert into tblEmployee49 values (6,'Ben', 4900, 'Female', 3) 

CREATE TABLE tblDepartment49
(
  DeptId int identity(1,1) primary key,
  DepartmentName nvarchar(1000)
)

Insert into tblDepartment49 values ('IT')
Insert into tblDepartment49 values ('Payroll')
Insert into tblDepartment49 values ('HR')
Insert into tblDepartment49 values ('Admin')

select * from tblEmployee49 
select * from tblDepartment49

ALTER TABLE tblEmployee49 add constraint FK_tblEmployee49_DepartmentId
FOREIGN KEY (DepartmentId) references tblDepartment49(DeptId)

WITH EmployeeCount49(DepartmentId, TotalEmployees)
as
(
		SELECT		DepartmentId, COUNT(*) as TotalEmployees
		FROM		tblEmployee49
		GROUP BY	DepartmentId
)
select		DepartmentName, TotalEmployees
from		tblDepartment49
JOIN		EmployeeCount49
ON			tblDepartment49.DeptId = EmployeeCount49.DepartmentId
ORDER BY	TotalEmployees

----------------------------------------------------------------------------------------------

WITH EmployeeCount49(DepartmentId, TotalEmployees)
as
(
		SELECT		DepartmentId, COUNT(*) as TotalEmployees
		FROM		tblEmployee49
		GROUP BY	DepartmentId
)
select 'Hello'

select		DepartmentName, TotalEmployees
from		tblDepartment49
JOIN		EmployeeCount49
ON			tblDepartment49.DeptId = EmployeeCount49.DepartmentId
ORDER BY	TotalEmployees

----------------------------------------------------------------------------------------------

WITH EmployeeCountBy_Payroll_IT_Dept(DepartmentName, Total)
as
(
			SELECT		DepartmentName, COUNT(Id) as TotalEmployees
			FROM		tblEmployee49
			JOIN		tblDepartment49
			ON			tblEmployee49.DepartmentId = tblDepartment49.DeptId
			WHERE		DepartmentName IN ('Payroll','IT')
			GROUP BY	DepartmentName
),
EmployeeCountBy_HR_Admin_Dept(DepartmentName, Total)
as
(
			select		DepartmentName, COUNT(Id) as TotalEmployees
			FROM		tblEmployee49
			JOIN		tblDepartment49
			ON			tblEmployee49.DepartmentId = tblDepartment49.DeptId
			WHERE		DepartmentName IN ('HR','Admin')
			GROUP BY	DepartmentName
)
select * from	EmployeeCountBy_HR_Admin_Dept
UNION
select * from	EmployeeCountBy_Payroll_IT_Dept
