--Advantages and Disadvantages of index in sql server PART - 38

--In Part 25, we have learnt that, Indexes are used by queries to find data quickly.

Create Table tblEmployee38
(
	[ID] int primary key,
	[FirstName] nvarchar(50),
	[LastName] nvarchar(50),
	[Salary] int,
	[Gender] nvarchar(50),
	[City] nvarchar(50)
)


select * from tblEmployee38

insert into tblEmployee38 values(1,'Mike','sandoz',4500,'Male','New York')
insert into tblEmployee38 values(2,'Sara','Meneo',6500,'Female','London')
insert into tblEmployee38 values(3,'John','Barber',2500,'Male','Sydney')
insert into tblEmployee38 values(4,'Pam','Grove',3500,'Female','Toronto')
insert into tblEmployee38 values(5,'James','Mirch',7500,'Male','London')



--SELECT STATMENT WITH A WHERE CLAUSE
select * from tblEmployee38 where Salary > 4000 and Salary < 8000


--DELETE AND PDATE STATMENT
Delete from tblEmployee38 where Salary = 2500
Update tblEmployee38 set Salary = 9000 where Salary = 7500

--ORDER BY ASENDING
select * from tblEmployee38 Order by Salary

--ORDER BY DESCENDING
select * from tblEmployee38 order by Salary DESC

--Group by
select Salary, COUNT(Salary) as Total
From tblEmployee38
Group by Salary

--Disadvantage of Indexes

--Additional Disk Space : Clustered Index does not, require any additional stroage. Every
--Non-Clustered index requires additional space as it is stored separately from the table.
--The amount of space required will depends on the size of the table, and the number and
--types of colimns used in the index.

--Insert Update and Delete statments can become slow : When DML (DATA mANIPULATION
--Language) statments (INSERT,UPDATE,DELETE) modifies data in a table, the data in all
--the indexes also needs to be updated. Indexes can help, to search and locate the rows,
--that we want to delete, but too may indexes to update can actually hurt the
--performance of data modifications.

--What is a covering query - If all the columns that you have requested in the SELECT
--clause of query, are present in the index, then there is no need to lookp in the table
--again. The requested columns data can simply be returned from the index.

--A Clustered index, always covers a query, since it contains all of the data in a table. A
--composite index is an index on two or more columns. Both clustered and non-clustered
--index can be composite indexes. To a certain extent, a composite index, can cover a 
--query.


