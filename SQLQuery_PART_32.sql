--MULTI-STATEMENT TABLE VALUED FUNCTION IN SQL SERVER PART - 32

--Multi-Statment Table Valued Functions

--	Multi statment table valued functions are very similar to Inline Table Valued function,
--	with a few differences
---------------------------------------------------------------------------------
--Inline Table Valued Function

create table tblEmployee32
(
	[Id] int primary key NOT NULL,
	[Name] nvarchar(50) NOT NULL,
	[Gender] nvarchar(50) NOT NULL,
	[DepartmentId] int NOT NULL
)
Create table tblDepartment32
(
	[ID] int primary key,
	[Dept_Name] nvarchar(50)
)

Alter table tblEmployee32 add DateOfBirth Date

select * from tblEmployee32
select * from tblDepartment32


Alter table tblEmployee32 add constraint FK_tblEmployee32_GenderId
FOREIGN KEY (DepartmentId) references tblDepartment32(ID)

insert into tblEmployee32 values(1,'sam','Male',1)
insert into tblEmployee32 values(2,'Ram','Male',1)
insert into tblEmployee32 values(3,'Sara','Female',3)
insert into tblEmployee32 values(4,'Todd','Male',2)
insert into tblEmployee32 values(5,'John','Male',3)
insert into tblEmployee32 values(6,'Sana','Female',2)
insert into tblEmployee32 values(7,'James','Male',1)
insert into tblEmployee32 values(8,'Rob','Male',2)
insert into tblEmployee32 values(9,'Steve','Male',1)
insert into tblEmployee32 values(10,'Pam','Female',2)


insert into tblDepartment32 values(1,'Devloper')
insert into tblDepartment32 values(2,'HR')
insert into tblDepartment32 values(3,'Account')

--Inline table line function

Create function fn_ILTVF_GetEmployees()
Returns Table
as
Return
select ID,Name,CAST(DateOfBirth as Date) as DOB from tblEmployee32

select * from [dbo].[fn_ILTVF_GetEmployees]()

update fn_ILTVF_GetEmployees() set Name = 'Sam1' where ID = 1

--------------------------------------------------------------------------
--  Multi-Statement Table Valued Function

Create Function fn_MSTVF_GetEmployees()
Returns @Table Table (ID int, Name nvarchar(30), DOB Date)
as 
Begin

	Insert into @Table 
	select ID, Name, CAST(DateOfBirth as Date) from tblEmployee32

	Return
END

select * from [dbo].[fn_MSTVF_GetEmployees]()

sp_helptext fn_MSTVF_GetEmployees

update fn_MSTVF_GetEmployees() set Name = 'Sam' where ID = 1

---------------------------------------------------------------

--DIFFERENCES INLINE TABLE VALUED FUNCTION AND MULTI STATEMENT TABLED VALUED FUNCTION

--1.	In an Inline Table Valued function, the RETURNS clause cannot contain the structure of the table,
--		the function returns. Where as, with the multi-statement table valued function, we specify the
--		structure of the table that gets returned

--2.	Inline Table Valued function cannot have BEGIN and END block, where as the multi-statement
--		function can have.

--3.	Inline Table valued function are better for performance, then multi-statment table valued
--		functions. If the given task, can be achieved using an inline table valued function, always perfer to
--		use them, over multi-stement table valued functions.

--4.	It's possible to update the underlying, using an inline table valued,but not possible
--		using multi-statment table valued funtion.
	
--Reason for improved performance of an inline table valued function:

--intemally, SQL Server treates an inlin table valued function much like it would a view and treats a 
--multi-statement table valued function similar to how it would a stroed procedure.

	


