--INLINE TABLE VALUED FUNCTION IN SQL SERVER PART - 31

--IN PART 30 of the video series we have seen how to creae and call 'Scalar user defined function'. In
--this part, we will learn about 'Inline Table Valued Function'

--SCALAR FUNCTION - RETURN A SCALAR VALUE
--INLINE TABLE VALUED FUNCTION - RETURNS A TABLE

--1.	We specify TABLE as the return type, instead of any scalar datatype
--2.	The function body is not enclosed between BEGIN and END block.
--3.	The structure of the table that gets returned, is determined by the SELECT statement with
--	    in the function

--Where can we use Inline Table Valued function
--1.	Inline Table Valued function can be used to achieve the functionality of parameterized views. We will
--		talk about views, in a leter session.
--2.	The table retruned by the table valued function, can also be used in joins with other tables

create table tblEmployee31
(
	[Id] int primary key NOT NULL,
	[Name] nvarchar(50) NOT NULL,
	[Gender] nvarchar(50) NOT NULL,
	[DepartmentId] int NOT NULL
)
Create table tblDepartment31
(
	[ID] int primary key,
	[Dept_Name] nvarchar(50)
)


select * from tblEmployee31
select * from tblDepartment31


Alter table tblEmployee31 add constraint FK_tblEmployee31_GenderId
FOREIGN KEY (DepartmentId) references tblDepartment31(ID)

insert into tblEmployee31 values(1,'sam','Male',1)
insert into tblEmployee31 values(2,'Ram','Male',1)
insert into tblEmployee31 values(3,'Sara','Female',3)
insert into tblEmployee31 values(4,'Todd','Male',2)
insert into tblEmployee31 values(5,'John','Male',3)
insert into tblEmployee31 values(6,'Sana','Female',2)
insert into tblEmployee31 values(7,'James','Male',1)
insert into tblEmployee31 values(8,'Rob','Male',2)
insert into tblEmployee31 values(9,'Steve','Male',1)
insert into tblEmployee31 values(10,'Pam','Female',2)


insert into tblDepartment31 values(1,'Devloper')
insert into tblDepartment31 values(2,'HR')
insert into tblDepartment31 values(3,'Account')





use[SQL-SERVER-PRAGIM-DATABASE]
GO
select * from tblEmployeThirtyOneTable

CREATE FUNCTION Fn_EmployeeByGender (@Gender nvarchar(10))
RETURNS TABLE
AS
RETURN (select Id, Name, DateOfBirth, Gender,DepartmentId 
		from tblEmployee31
		where GEnder = @Gender)


select  Name, Gender, Dept_Name
from   	Fn_EmployeeByGender('Male') as Emp
JOIN	tblDepartment31 D ON D.Id = Emp.DepartmentId


select * from Fn_EmployeeByGender('Female')

ALTER TABLE tblEmployee31 add DateOfBirth Date

--Where can we use Inline Table Valued function

--1.	Inline Table Valued function can be used to
--		achieve the functionality of parameterized
--		views. We will talk about views, in a later 
--		session.
--2.	The table returned by the table valued function, 
--		can also be used in join with other tables
	
--	JOIN
--	Part 12 : Join in sql server
--	Part 13 : Advanced Joins
----------------------------------------------------------

-- CALLING FUNCTION

select * from Fn_EMployeeByGender('Male')

select * from Fn_EMployeeByGender('Female')

----------------------------------------------------------


